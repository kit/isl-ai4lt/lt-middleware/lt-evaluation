#!/bin/bash
set -eu  # Crash if variable used without being set

ARTIFACT_PATH=${ARTIFACT_PATH:-$(pwd)}

# Function to convert time format HH:MM:SS to seconds
time_to_seconds() {
  local h=$(echo $1 | cut -d: -f1)
  local m=$(echo $1 | cut -d: -f2)
  local s=$(echo $1 | cut -d: -f3 | awk -F. '{print $1}') # Remove milliseconds
  echo $((10#$h * 3600 + 10#$m * 60 + 10#$s))
}

declare -a datanames=( "IWSLT.ACLdev2023-en-de" "IWSLT2019-test-en-de" "IWSLT2020-test-en-de" "mTEDx-es-en" "mTEDx-fr-en" "mTEDx-pt-en" "mTEDx-it-en" "LT.nonCS.2012-de-en" "LT.CS.2012-de-en" )

for dataname in ${datanames[@]}; do
  data_root_path="${ARTIFACT_PATH}/data"
  processed_data_dir=${dataname}_proccessed
  src_lang=$(echo ${dataname} | awk -F'-' '{print $(NF-1)}')
  tgt_lang=$(echo ${dataname} | awk -F'-' '{print $NF}')
  nr_audio_files=$(ls ${data_root_path}/${processed_data_dir}/SRCAUDIO.${src_lang}/audio_*.wav | wc -l )

  total_audio_length=0
  for ((i=0;i<${nr_audio_files};i++)); do
    srcaudio_file=${data_root_path}/${processed_data_dir}/SRCAUDIO.${src_lang}/audio_${i}.wav
    length=$(ffmpeg -i "$srcaudio_file" 2>&1 | grep "Duration" | awk '{print $2}' | tr -d ,)
    length_seconds=$(time_to_seconds "$length")
    total_audio_length=$((total_audio_length + length_seconds))
  done

  total_audio_length=$(bc <<< "scale=2; $total_audio_length / 3600")

  TGT_TEXT=${data_root_path}/${processed_data_dir}/TGTTEXT.${tgt_lang}/TGTTEXT.txt
  nr_utterance=$(wc -l "$TGT_TEXT" | awk '{print $1}')

  echo "--------------------------------------------------------------------------------------------------"
  echo ${dataname}
  echo "Audio length: ${total_audio_length}"
  echo "# Utterances: ${nr_utterance}"
done