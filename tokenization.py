#!/usr/bin/env python3
from sacrebleu.tokenizers import TOKENIZERS
import argparse
from utils import load_text_file, write_text_file


def tokenize(lang, sentence):
    if lang == 'zh':
        tokenizer = TOKENIZERS['zh']()
        sentence_tok = tokenizer(sentence)
    elif lang == 'ja':
        tokenizer = TOKENIZERS['ja-mecab']()
        sentence_tok = tokenizer(sentence)
    elif lang == 'ko':
        tokenizer = TOKENIZERS['ko-mecab']()
        sentence_tok = tokenizer(sentence)
    else:
        tokenizer = TOKENIZERS['13a']()
        sentence_tok = tokenizer(sentence)
    return sentence_tok


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file",
                        help="Text file to be tokenized.", type=str, default=None)
    parser.add_argument("--output_file",
                        help="File to store the tokenized text",
                        type=str, default=None)
    parser.add_argument("--lang", type=str, default=None)
    args = parser.parse_args()

    sentences = load_text_file(args.input_file)
    sentences_tok = [tokenize(args.lang, x) for x in sentences]
    write_text_file(sentences_tok, args.output_file)
