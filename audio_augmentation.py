from datasets import load_dataset, concatenate_datasets
import random
import numpy as np
import torch
from signal_processing import rescale, apply_reverb  # https://gist.github.com/nguyenvulebinh/2971966868fa8b93ce50f94d3dbfa0ec
from copy import deepcopy
import torchaudio


def load_noise_dataset(ignore_speech=False):
    addnoise_dataset = load_dataset('nguyenvulebinh/addnoise')
    whamnoise_dataset = load_dataset('nguyenvulebinh/wham')['train']
    whamnoise_dataset_remove_columns = whamnoise_dataset.column_names
    whamnoise_dataset_remove_columns.remove('audio')
    whamnoise_dataset = whamnoise_dataset.remove_columns(whamnoise_dataset_remove_columns)
    addnoise_dataset['noise_audio'] = concatenate_datasets([addnoise_dataset['noise_audio'], whamnoise_dataset])
    if ignore_speech:
        addnoise_dataset = concatenate_datasets([addnoise_dataset['noise_audio'], addnoise_dataset['music_audio']])
    else:
        addnoise_dataset = concatenate_datasets(list(addnoise_dataset.values()))
    # print(addnoise_dataset)

    reverb_dataset = load_dataset('nguyenvulebinh/reverb')['train']
    return addnoise_dataset, reverb_dataset


def augment(audio,
            noise_dataset=None,
            reverb_dataset=None,
            add_noise_ratio=0.2,
            add_reverb_ratio=0.2,
            idx=None,
            save_debug_audio_path=None,
            output_audio_path=None):
    augment_noise = False
    augment_reverb = False
    raw_audio = deepcopy(audio)

    if noise_dataset is not None and random.random() < add_noise_ratio:
        noise = torch.from_numpy(noise_dataset[random.randint(0, len(noise_dataset) - 1)]['audio']['array']).float()
    else:
        noise = None

    ##############DEBUG################
    if save_debug_audio_path is not None and idx is not None:
        import soundfile as sf
        import os

        sf.write(os.path.join(save_debug_audio_path, f'{idx}_audio_raw.wav'), audio.numpy(), 16000)
        if noise is not None:
            sf.write(os.path.join(save_debug_audio_path, f'{idx}_noise_raw.wav'), noise.numpy(), 16000)
    ###################################

    # random change db level
    target_lvl = np.clip(random.normalvariate(-20.43, 15.57), -27, -17)
    audio = rescale(audio, torch.tensor(audio.size(-1)), target_lvl, scale="dB")
    if noise is not None:
        noise_lvl = np.clip(random.normalvariate(-20.43, 15.57), -40, -20)
        noise = rescale(noise, torch.tensor(noise.size(-1)), noise_lvl, scale="dB")

    ##############DEBUG################
    if save_debug_audio_path is not None and idx is not None:
        sf.write(os.path.join(save_debug_audio_path, '{}_audio_norm_{:.2f}.wav'.format(idx, target_lvl)), audio.numpy(),
                 16000)
        if noise is not None:
            sf.write(os.path.join(save_debug_audio_path, '{}_noise_norm_{:.2f}.wav'.format(idx, noise_lvl)),
                     noise.numpy(), 16000)
    ###################################

    # apply reverb
    if reverb_dataset is not None and random.random() < add_reverb_ratio:
        augment_reverb = True
        reverb = torch.from_numpy(reverb_dataset[random.randint(0, len(reverb_dataset) - 1)]['audio']['array']).float()
        audio, reverb_len = apply_reverb(audio, reverb)
        if noise is not None:
            noise, noise_reverb_len = apply_reverb(noise, reverb)

        ##############DEBUG################
        if save_debug_audio_path is not None and idx is not None:
            sf.write(os.path.join(save_debug_audio_path, '{}_audio_reverb_{:.2f}.wav'.format(idx, reverb_len)),
                     audio.numpy(), 16000)
            if noise is not None:
                sf.write(
                    os.path.join(save_debug_audio_path, '{}_noise_reverb_{:.2f}.wav'.format(idx, noise_reverb_len)),
                    noise.numpy(), 16000)
        ###################################

    # strip or pad noise to audio length
    if noise is not None:
        if len(audio) > len(noise):
            noise_pad = torch.zeros((audio.size(-1),))
            start = random.randint(0, audio.size(-1) - noise.size(-1))
            noise_pad[start:start + noise.size(-1)] = noise
        else:
            noise_pad = noise[:audio.size(-1)]
        noise = noise_pad

    # sum signal
    if noise is not None:
        augment_noise = True
        audio = audio + noise

    ##############DEBUG################
    if save_debug_audio_path is not None and idx is not None:
        sf.write(os.path.join(save_debug_audio_path, f'{idx}_audio_output.wav'), audio.numpy(), 16000)

    if output_audio_path is not None:
        import soundfile as sf
        import os
        sf.write(output_audio_path, audio.numpy(), 16000)

    if not augment_reverb and not augment_noise:
        return raw_audio

    return audio