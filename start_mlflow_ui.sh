#!/bin/bash

ARTIFACT_PATH=${ARTIFACT_PATH:-$(pwd)}
mlflow ui --port 6767 --host 0.0.0.0 --default-artifact-root ${ARTIFACT_PATH}/mlflow/mlruns --backend-store-uri ${ARTIFACT_PATH}/mlflow/mlruns
