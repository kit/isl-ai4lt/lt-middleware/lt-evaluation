# LT-evaluation

## Installation
Install the required Python packages provided in `requirements.txt`

First activate the Python environment where you install the packages.
Then set the required variable.
An example can be found in the file `set_path_vars_and_env.sh`. Running the file with
```bash
. set_path_vars_and_env.sh
```
to set the variables.

## Nightly test
Open crontab file by `crontab -e`, then add this to the end of the file to run the script at midnight everyday:
```bash
0 0 * * * ${REPO_ROOT_PATH}/run_evaluate_pipeline_different_settings.sh "https://${USERNAME}:${PASSWORD}@lt2srv.iar.kit.edu" nightly 2>&1 | tee ${ARTIFACT_PATH}/nightly_log/main_server.log
0 0 * * * ${REPO_ROOT_PATH}/run_evaluate_pipeline_different_settings.sh "https://${USERNAME}:${PASSWORD}@lt2srv-backup.iar.kit.edu" nightly 2>&1 | tee ${ARTIFACT_PATH}/nightly_log/backup_server.log
```
Comment out these two lines in the crontab file to stop running it at midnight.


## Reproducing experiments in the system demo paper
### Quality vs Latency trade-off with LA2_chunk_size
```bash
. run_evaluate_pipeline_different_settings.sh "https://${USERNAME}:${PASSWORD}@lt2srv.iar.kit.edu" LA2_chunk_size
```

### Overall performance (cascaded vs end-to-end, offline vs fixed mode vs revision mode on different language pairs)
```bash
. run_evaluate_pipeline_different_settings.sh "https://${USERNAME}:${PASSWORD}@lt2srv.iar.kit.edu" onl-off-resending
```

### Parallel speech component
```bash
. run_parallel_test.sh "https://${USERNAME}:${PASSWORD}@lt2srv.iar.kit.edu"
```

### Parallel text component
```bash
. run_evaluate_pipeline_different_settings.sh "https://${USERNAME}:${PASSWORD}@lt2srv.iar.kit.edu" mulMT
```

## Start up mlflow UI to interact with experiment results.
```bash
. start_mlflow_ui.sh
```