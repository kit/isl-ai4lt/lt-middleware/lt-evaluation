#!/usr/bin/env python3
import argparse
import requests
import json
import time
import sys
import threading
from sseclient import SSEClient


def collectTranslations(url, sessionID, translations):
    print(url)
    print(sessionID)
    messages = SSEClient(url + "/ltapi/stream?channel=" + sessionID)
    for msg in messages:
        print("Got message:", msg)
        data = json.loads(msg.data)
        translations.append(msg.data)
        if ("controll" in data and data["controll"] == "END"):
            break;


def translate(url, source_file, sessionID, streamID, output_file=None, verbose=1, mt_properties=None):
    # More flexible code
    """if (sessionID == "-1"):
        id = requests.post(url + "/ltapi/startsession")
        sessionID = id.text
        print("Start ID: ", sessionID);
    if (streamID == "-1"):
        stream = requests.post(url + "/ltapi/" + sessionID + "/addstream")
        streamID = stream.text;

    g = {}
    g["user:"+streamID] = ["log", "mt:0"]
    g["mt:0"] = ["log", "api"]
    stream = requests.post(url + "/ltapi/" + sessionID + "/setgraph", json=json.dumps(g));"""

    d = {"language":mt_properties["language"]} if "language" in mt_properties else {}

    res = requests.post(url + "/ltapi/get_default_mt", json=json.dumps(d));
    if res.status_code != 200:
        print("ERROR in requesting default graph for MT")
        sys.exit(1)
    sessionID, streamID = res.text.split()

    p = {}
    p["mt:0"] = mt_properties
    res = requests.post(url + "/ltapi/" + sessionID + "/setproperties", json=json.dumps(p));

    translations = []
    translations = []
    x = threading.Thread(target=collectTranslations, args=(url, sessionID, translations))
    x.start()

    data={'controll':"INFORMATION"}
    stream = requests.post(url + "/ltapi/" + sessionID + "/" + streamID + "/append", json=json.dumps(data));


    f = open(source_file)
    l = f.readline()
    time = 0
    while (l):
        if verbose > 0:
            print(l.strip());
        data = {'seq': l.strip() if mt_properties["mode"]=="SendStable" else " ".join(l.strip().split()[1:])}
        data["start"] = time
        data["end"] = time + len(l.strip().split());
        data["unstable"] = False if mt_properties["mode"]=="SendStable" else l.strip().split()[0] == "Unstable"
        time += len(l.strip().split()) + 1;
        stream = requests.post(url + "/ltapi/" + sessionID + "/" + streamID + "/append", json=json.dumps(data));
        if mt_properties["mode"]!="SendStable":
            time.sleep(1)
        l = f.readline()
    data = {'controll': "END"};
    stream = requests.post(url + "/ltapi/" + sessionID + "/" + streamID + "/append", json=json.dumps(data));

    x.join()

    if verbose > 0:
        print("Translations:")
        print(translations)

    if output_file is not None:
        with open(output_file, 'w') as f:
            json.dump(translations, f)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-u",
                        "--url",
                        default="http://localhost:5000",
                        help="Inference URL of the ASR worker")
    parser.add_argument("--source-file",
                        help="Path to the soruce text", type=str)
    parser.add_argument("--session",
                        help="Session ID to use", type=str, default="-1")
    parser.add_argument("--stream",
                        help="stream to use", type=str, default="-1")
    parser.add_argument('--mt-kv', action='append', type=lambda kv: kv.split('='), dest='mt_properties', default=[['mode', 'SendStable']],
                        help='Used mt properties, e.g. '
                             '--mt-kv mode=SendStable '
                             '--mt-kv mt_server_${src_lang}-${tgt_lang}=http://URL:PORT/SOMETHING')
    parser.add_argument("--verbose",
                        help="0: do not print out the translations. 1: print translations", type=int, default=1)
    parser.add_argument("--output-file",
                        help="Path to the file to save the output translations", type=str, default=None)
    args = parser.parse_args()

    mt_properties = {}
    for k,v in args.mt_properties:
        mt_properties[k] = v
    args.mt_properties = mt_properties

    print(args)

    # Example input format for unstable
    #     Stable this is
    #     Unstable the
    #     Stable a
    #     Unstable text
    #     Unstable more sentences.
    #     Stable sentence.
    #     Unstable And more
    #     Stable And more to come.

    translate(url=args.url, source_file=args.source_file, sessionID=args.session, streamID=args.stream,
              output_file=args.output_file, verbose=args.verbose, mt_properties=args.mt_properties)
