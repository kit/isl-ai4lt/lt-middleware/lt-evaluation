from pathlib import Path

import pandas as pd

FILE_BASE = "yt_seg"
PARTITION_BASE = FILE_BASE + ".{}"

def _read_segmentation_json(data_file):
    data_df = pd.read_json(data_file, orient='records', lines=True)
    data_df["targets"] = data_df["targets"].str.lstrip("|=") # to enforce string type, in order to avoid truncation of leading zeros
    return data_df

def get_data(data_dir):
    data_file = Path(data_dir, f'{FILE_BASE}.json')
    return _read_segmentation_json(data_file)

def get_partition(partition, data_dir):
    data_file = Path(data_dir, "partitions", PARTITION_BASE.format(partition) + '.json')
    return _read_segmentation_json(data_file)

FILE_BASE_TITLE = "yt_seg_titles.{}"

def get_title_partition(partition, data_dir):
    data_file = Path(data_dir, "partitions", FILE_BASE_TITLE.format(partition) + '.json')
    data_df = pd.read_json(data_file, orient='records', lines=True)
    return data_df