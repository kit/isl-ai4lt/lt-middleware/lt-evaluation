import mlflow
import argparse
from datetime import datetime
from collections import defaultdict


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--function",
                        help="Start the run, or log all metrics and end run.", type=str,
                        choices=['start_run', 'log_and_end_run'], required=True)
    parser.add_argument("--params_path",
                        help="Path to the file storing the parameters", type=str)
    parser.add_argument("--metrics_paths",
                        help="Paths to the files storing the metrics", type=str, nargs="*")
    parser.add_argument("--artifacts_path",
                        help="Path to the folder storing the output artifacts", type=str)
    parser.add_argument("--run_id",
                        help="The run_id created before when function=start_run. Used when function=log_and_end_run",
                        type=str)
    parser.add_argument("--run_name",
                        help="The run_id created before when function=start_run. Used when function=log_and_end_run",
                        type=str, default=None)
    parser.add_argument("--experiment_name",
                        help="Experiment is a group of runs. experiment_name can be "
                             "['nightly_test', 'all_configs_test', 'LT_eval']",
                        default='LT_eval',
                        type=str)
    parser.add_argument("--local_mlflow_path", default="mlflow/mlruns/", type=str)
    args = parser.parse_args()

    # mlflow.set_tracking_uri("http://i13srv53:6767")
    mlflow.set_tracking_uri(args.local_mlflow_path)

    if args.function == 'start_run':
        # Load in the parameters
        with open(args.params_path, 'r') as f:
            lines = f.readlines()
            lines = [line.strip() for line in lines if (not line.startswith('#')) and ('=' in line)]
            params = [line.split('=') for line in lines]
            params = dict(params)

        # Start a run
        mlflow.set_experiment(experiment_name=args.experiment_name)
        if args.run_name is None:
            run_name = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        else:
            run_name = args.run_name
        with mlflow.start_run(run_name=run_name):
            # Log a parameters (key-value pair)
            for key, value in params.items():
                mlflow.log_param(key, value)
            print(mlflow.active_run().info.run_id)

    elif args.function == 'log_and_end_run':
        # Load in the metrics
        all_metrics = []
        for metrics_path in args.metrics_paths:
            with open(metrics_path, 'r') as f:
                lines = f.readlines()
                lines = [line.strip() for line in lines]
                metrics = [line.split('=') for line in lines if '=' in line]
            all_metrics.extend(metrics)
        all_metrics = dict(all_metrics)
        all_metrics = calculate_and_add_averages(all_metrics)

        run_id = args.run_id
        with mlflow.start_run(run_id=run_id):
            # Log metrics (key-value pair)
            for key, value in all_metrics.items():
                if ("latency" in key) and (mlflow.active_run().data.params["version"] == "offline"):
                    # Do not log latency for offline mode
                    continue
                mlflow.log_metric(key, value)

            # Log output artifacts
            mlflow.log_artifacts(args.artifacts_path)

    else:
        raise RuntimeError(f"Unknown function {args.function}")


def calculate_and_add_averages(data):
    """
    Average the Chaptering metrics over different audios
    """
    # Step 1: Identify key prefixes and group values
    prefix_groups = defaultdict(list)

    for key, value in data.items():
        if "Chaptering" in key:
            parts = key.split('_')
            if len(parts) > 2:
                prefix = '_'.join(parts[:-2])  # Last 2 are audio ID and lang
                prefix_groups[prefix].append(float(value))

    # Step 2: Calculate the average for each prefix group
    averages = {}
    for prefix, values in prefix_groups.items():
        average = sum(values) / len(values)
        averages[prefix] = average

    # Step 3: Add the averages back to the dictionary
    data.update(averages)

    return data


if __name__ == "__main__":
    main()
