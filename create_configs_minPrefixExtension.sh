#!/bin/bash

if [ -z "$1" ]; then
  configs_path=configs
else
  configs_path=$1
fi

# Store different settings to seperate files in configs/ folder
mkdir -p ${configs_path}
rm ${configs_path}/*.sh
declare -a datanames=( "IWSLT.ACLdev2023-en-de" "IWSLT2019-test-en-de" )

asr_server="ASR_denoise_iso"
mt_server="mdeltaLM_iso"
LA2_chunk_size=2
use_prep="False"

config_counter=0
for dataname in ${datanames[@]}; do
  # Evaluate cascaded ST
  # Online settings
  declare -a stability_detections=( "True" )
  declare -a resendings=( "False" )
  if [[ $resending == "True" ]]; then
    ASRmode="SendUnstable"
    MTmode="SendUnstable"
  else
    ASRmode="SendStable"
    MTmode="SendPartial"
  fi
  declare -a minPrefixExtensions=( 1 2 5 )
  for stability_detection in ${stability_detections[@]}; do
    for resending in ${resendings[@]}; do
      for minPrefixExtension in ${minPrefixExtensions[@]}; do
          echo "dataname=${dataname}
task=cascadedST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
MTmode=${MTmode}
use_prep=${use_prep}
asr_server=${asr_server}
mt_server=${mt_server}
resending=${resending}
minPrefixExtension=${minPrefixExtension}
LA2_chunk_size=${LA2_chunk_size}" >> ${configs_path}/config${config_counter}.sh
          config_counter=$((config_counter+1))
      done
    done
  done
done