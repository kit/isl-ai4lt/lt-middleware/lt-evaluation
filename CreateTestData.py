import json
import re

import requests
import os
import tarfile
import argparse
import pandas as pd
import subprocess
import xml.dom.minidom
from utils import str_to_bool, none_or_str
from sacremoses import MosesDetokenizer
import yaml
from load_data_ytseg import get_partition
import shutil


try:
    from audio_augmentation import load_noise_dataset, augment
except ModuleNotFoundError:
    load_noise_dataset = None
    augment = None

try:
    import torchaudio
except ModuleNotFoundError:
    torchaudio = None


def augment_audio(noise_type, input_audio_path, output_audio_path):
    addnoise_dataset, reverb_dataset = load_noise_dataset()
    audio, rate = torchaudio.load(input_audio_path)

    if noise_type == 'echo':
        augment(audio.squeeze(),
                noise_dataset=addnoise_dataset,
                reverb_dataset=reverb_dataset,
                add_noise_ratio=0,
                add_reverb_ratio=1.,
                # idx=1,  # just for debug
                # save_debug_audio_path='./output'  # just for debug
                output_audio_path=output_audio_path,
                )
    elif noise_type == 'noise':
        augment(audio.squeeze(),
                noise_dataset=addnoise_dataset,
                reverb_dataset=reverb_dataset,
                add_noise_ratio=1.,
                add_reverb_ratio=0,
                # idx=1,  # just for debug
                # save_debug_audio_path='./output'  # just for debug
                output_audio_path=output_audio_path,
                )
    elif noise_type == 'echonoise':
        augment(audio.squeeze(),
                noise_dataset=addnoise_dataset,
                reverb_dataset=reverb_dataset,
                add_noise_ratio=1.,
                add_reverb_ratio=1.,
                # idx=1,  # just for debug
                # save_debug_audio_path='./output'  # just for debug
                output_audio_path=output_audio_path,
                )
    else:
        raise RuntimeError(f'Unknown noise_type {noise_type}')


def detokenize(tokenized_str, lang):
    detokenizer = MosesDetokenizer(lang=lang)
    return detokenizer.detokenize(tokenized_str.split())


def download_data(dataname='newstest2019',
                  data_root_path='data',
                  src_langs='en',
                  tgt_langs='de'):
    if 'newstest2019' in dataname:
        if not os.path.isdir(f"{data_root_path}/wmt19-submitted-data-v3"):
            data_url = 'http://ufallab.ms.mff.cuni.cz/~bojar/wmt19/wmt19-submitted-data-v3-txt-minimal.tgz'
            zipped_data_path = f"{data_root_path}/wmt19-submitted-data-v3-txt.tgz"
            unzip_tar(data_root_path, data_url, zipped_data_path)

    elif "covost2-test" in dataname:
        if not (src_langs == 'en' and tgt_langs == 'de'):
            raise RuntimeError(f"Dataset {dataname} only available for src_lang=en, tgt_lang=de")

        if not os.path.isdir(f"{data_root_path}/covost2-test-{src_langs}-{tgt_langs}"):
            data_url = f'https://bwsyncandshare.kit.edu/s/cQt2CCkNFnJw9Xt/download/covost2-test-{src_langs}-{tgt_langs}.tar.gz'
            zipped_data_path = f"{data_root_path}/covost2-test-{src_langs}-{tgt_langs}.tar.gz"
            unzip_tar(data_root_path, data_url, zipped_data_path)

    # elif re.match(r"IWSLT\d{4}-test-..-..", dataname):
    #     assert len(src_langs.split(',')) == 1 and len(tgt_langs.split(',')) == 1
    #
    #     # Extract the year
    #     year = dataname[5:9]
    #     if not os.path.isdir(f"{data_root_path}/IWSLT.tst{year}"):
    #         data_url = f'http://i13pc106.ira.uka.de/~jniehues/IWSLT-SLT/data/eval/{src_langs}-{tgt_langs}/IWSLT-SLT.tst{year}.{src_langs}-{tgt_langs}.tgz'
    #         zipped_data_path = f"{data_root_path}/IWSLT-SLT.tst{year}.{src_langs}-{tgt_langs}.tgz"
    #         unzip_tar(data_root_path, data_url, zipped_data_path)

    elif dataname == "IWSLT2019-test-en-de":
        if not os.path.isdir(f"{data_root_path}/IWSLT.tst2019"):
            data_url = "https://bwsyncandshare.kit.edu/s/D9X3JqBtcyrEN3j/download/IWSLT-SLT.tst2019.en-de.tgz"
            zipped_data_path = f"{data_root_path}/IWSLT-SLT.tst2019.en-de.tgz"
            unzip_tar(data_root_path, data_url, zipped_data_path)

    elif dataname == "IWSLT2020-test-en-de":
        if not os.path.isdir(f"{data_root_path}/IWSLT.tst2020"):
            data_url = "https://bwsyncandshare.kit.edu/s/nbNaZtkNCrsJPtB/download/IWSLT-SLT.tst2020.en-de.tgz"
            zipped_data_path = f"{data_root_path}/IWSLT-SLT.tst2020.en-de.tgz"
            unzip_tar(data_root_path, data_url, zipped_data_path)

    elif dataname == "IWSLT2014-test-de-en":
        if not os.path.isdir(f"{data_root_path}/IWSLT.tst2014"):
            data_url = "https://bwsyncandshare.kit.edu/s/Ck4JEBzgqRYXcdy/download/IWSLT-SLT.tst2014.de-en.tgz"
            zipped_data_path = f"{data_root_path}/IWSLT-SLT.tst2014.de-en.tgz"
            unzip_tar(data_root_path, data_url, zipped_data_path)

    elif 'IWSLT.ACLdev2023' in dataname:
        if src_langs != 'en':
            raise RuntimeError(f"src_langs {src_langs} not available ")
        if not os.path.isdir(f"{data_root_path}/IWSLT.ACLdev2023"):
            data_url = f'http://i13pc106.ira.uka.de/~jniehues/IWSLT-SLT/data/eval/{src_langs}-xx/IWSLT-SLT.ACLdev2023.{src_langs}-xx.tgz'
            zipped_data_path = f"{data_root_path}/IWSLT-SLT.ACLdev2023.{src_langs}-xx.tgz"
            unzip_tar(data_root_path, data_url, zipped_data_path)

    elif 'mTEDx' in dataname:
        if not os.path.isdir(f"{data_root_path}/mtedx/{src_langs}-{tgt_langs}"):
            data_url = f'https://openslr.elda.org/resources/100/mtedx_{src_langs}-{tgt_langs}.tgz'
            zipped_data_path = f"{data_root_path}/mtedx_{src_langs}-{tgt_langs}.tgz"
            unzip_tar(f"{data_root_path}/mtedx", data_url, zipped_data_path)

    elif 'YTSeg' in dataname:
        if not os.path.isdir(f"{data_root_path}/ytseg"):
            bashCommand = f"git lfs clone https://huggingface.co/datasets/retkowski/ytseg {data_root_path}"
            process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
            output, error = process.communicate()


def unzip_tar(data_root_path, data_url, zipped_data_path):
    response = requests.get(data_url)
    with open(zipped_data_path, "wb") as f:
        f.write(response.content)
    # Unzip the data
    tar = tarfile.open(zipped_data_path, "r:gz")
    tar.extractall(data_root_path)
    tar.close()


def format_as_full_sentence(text: str, end_punctuations: list = [".","?","!",":"]) -> str:
    """
    Reformat the text to turn it to a full sentence, i.e., end punctuation in the end and no end punctuation in the
    middle
    :param text: a line of input in the SRC file
    :return: formatted text as a full sentence
    """

    # Add a dot if text does not end with an end-punctuation
    if text[-1] not in end_punctuations:
        text = text + '.'

    # Remove all end-punctuations that are in the middle of the text
    text_body = text[:-1]
    for punct in end_punctuations:
        text_body = text_body.replace(punct, '')
    text = text_body + text[-1]

    return text


def process_data(dataname, data_root_path, processed_data_dir=None, src_langs='en', tgt_langs='de',
                 audio_augmentation=None):
    """
    :param dataname:
    :param data_root_path:
    :param processed_data_dir: save the processed data to {data_root_path}/{processed_data_dir}
    :return:
    """
    if processed_data_dir is None:
        if audio_augmentation is None:
            processed_data_dir = f"{dataname}_processed"
        else:
            processed_data_dir = f"{dataname}_processed_add{audio_augmentation}"
    if os.path.isdir(f"{data_root_path}/{processed_data_dir}"):
        return

    if 'newstest2019' in dataname:
        assert len(src_langs.split(',')) == 1 and len(tgt_langs.split(',')) == 1

        data_components = ['srctext', 'tgttext']

        original_srctext_file_path = f"{data_root_path}/wmt19-submitted-data-v3/txt/sources/newstest2019-{src_langs}{tgt_langs}-src.{src_langs}"
        original_tgttext_file_path = f"{data_root_path}/wmt19-submitted-data-v3/txt/references/newstest2019-{src_langs}{tgt_langs}-ref.{tgt_langs}"

        srctext_lines_alllangs = {}
        tgttext_lines_alllangs = {}

        with open(original_srctext_file_path, 'r') as f:
            srctext_lines_flat = f.readlines()
            srctext_lines_flat = [srctext_line.strip() for srctext_line in srctext_lines_flat]
            srctext_lines_alllangs[src_langs] = srctext_lines_flat

        with open(original_tgttext_file_path, 'r') as f:
            tgttext_lines_flat = f.readlines()
            tgttext_lines_flat = [tgttext_line.strip() for tgttext_line in tgttext_lines_flat]
            tgttext_lines_alllangs[tgt_langs] = tgttext_lines_flat

    elif "covost2-test" in dataname:
        assert len(src_langs.split(',')) == 1 and len(tgt_langs.split(',')) == 1

        if not (src_langs == 'en' and tgt_langs == 'de'):
            raise RuntimeError(f"Dataset {dataname} only available for src_langs={src_langs}, tgt_langs={tgt_langs}")

        data_components = ['srcaudio', 'srctext', 'tgttext']

        test_df = pd.read_csv(f"{data_root_path}/covost2-test-{src_langs}-{tgt_langs}/test.tsv", index_col=0)
        srctext_lines_flat = test_df['sentence'].to_list()
        tgttext_lines_flat = test_df['translation'].to_list()
        srcaudio_paths = test_df['path'].apply(
            lambda x: f"{data_root_path}/covost2-test-{src_langs}-{tgt_langs}/{src_langs}/clips/{x}"
        )

        srctext_lines_alllangs = {src_langs: srctext_lines_flat[:2]}
        tgttext_lines_alllangs = {tgt_langs: tgttext_lines_flat[:2]}
        srcaudio_paths_alllangs = {src_langs: srcaudio_paths[:2]}

    elif "IWSLT" in dataname:

        data_components = ['srcaudio', 'srctext', 'tgttext']
        if "IWSLT2014-test" in dataname:
            assert len(src_langs.split(',')) == 1 and len(tgt_langs.split(',')) == 1

            if not (src_langs == 'de' and tgt_langs == 'en'):
                raise RuntimeError(f'Use IWSLT2014-test only for de-en')
            data_folder = f"{data_root_path}/IWSLT.tst2014"
            srctext_xml = f"{data_folder}/IWSLT.TED.tst2014.de-en.de.xml"
            tgttext_xml = f"{data_folder}/IWSLT.TED.tst2014.de-en.en.xml"
            audio_folder = f"{data_folder}/wavs"

            if not (os.path.isdir(data_folder) and os.path.isdir(audio_folder)):
                raise RuntimeError(f"{dataname} not stored locally. Please try another dataset instead.")

            with open(f"{data_folder}/CTM_LIST", 'r') as f:
                srcaudio_ids = f.readlines()
                srcaudio_ids = [x.split("/")[1].split(".")[2] for x in srcaudio_ids]

                srcaudio_paths = []
                for srcaudio_id in srcaudio_ids:
                    for file_path in os.listdir(audio_folder):
                        if srcaudio_id in file_path:
                            srcaudio_paths.append(f"{audio_folder}/{file_path}")
                            break

            srctext_xml_alllangs = {src_langs: srctext_xml}
            tgttext_xml_alllangs = {tgt_langs: tgttext_xml}
            srcaudio_paths_alllangs = {src_langs: srcaudio_paths}

        elif re.match(r"IWSLT\d{4}-test-..-..", dataname):
            assert len(src_langs.split(',')) == 1 and len(tgt_langs.split(',')) == 1

            # Extract the year
            year = dataname[5:9]
            data_folder = f"{data_root_path}/IWSLT.tst{year}"
            audio_folder = f"{data_folder}/wavs"
            srctext_xml = f"{data_folder}/IWSLT.TED.tst{year}.{src_langs}-{tgt_langs}.{src_langs}.xml"
            tgttext_xml = f"{data_folder}/IWSLT.TED.tst{year}.{src_langs}-{tgt_langs}.{tgt_langs}.xml"

            with open(f"{data_folder}/FILE_ORDER", 'r') as f:
                srcaudio_names = f.readlines()
                srcaudio_names = [x.strip() for x in srcaudio_names]
                srcaudio_paths = [f"{audio_folder}/{x}.wav" for x in srcaudio_names]

            srctext_xml_alllangs = {src_langs: srctext_xml}
            tgttext_xml_alllangs = {tgt_langs: tgttext_xml}
            srcaudio_paths_alllangs = {src_langs: srcaudio_paths}

        elif "IWSLT.ACLdev2023" in dataname:
            assert len(src_langs.split(',')) == 1

            data_folder = f"{data_root_path}/IWSLT.ACLdev2023"
            audio_folder = f"{data_folder}/full_wavs"

            srctext_xml_alllangs = {}
            tgttext_xml_alllangs = {}
            for src_lang in src_langs.split(','):
                srctext_xml_alllangs[src_lang] = f"{data_folder}/text/IWSLT.ACL.ACLdev2023.{src_lang}-xx.{src_lang}.xml"
                for tgt_lang in tgt_langs.split(','):
                    tgttext_xml_alllangs[tgt_lang] = f"{data_folder}/text/IWSLT.ACL.ACLdev2023.{src_lang}-xx.{tgt_lang}.xml"

            with open(f"{data_folder}/FILE_ORDER", 'r') as f:
                srcaudio_names = f.readlines()
                srcaudio_names = [x.strip() for x in srcaudio_names]
                srcaudio_paths = [f"{audio_folder}/{x}.wav" for x in srcaudio_names]

            srcaudio_paths_alllangs = {src_langs: srcaudio_paths}

        elif "IWSLT.dev2012" in dataname:
            assert len(src_langs.split(',')) == 1 and len(tgt_langs.split(',')) == 1

            if not (src_langs == 'de' and tgt_langs == 'en'):
                raise RuntimeError(f'Use IWSLT.dev2012 only for de-en')
            data_folder = f"/home/jniehues/data/SLT/de-en/final/IWSLT.dev2012"
            srctext_xml = f"{data_folder}/IWSLT.TED.dev2012.de-en.de.xml"
            tgttext_xml = f"{data_folder}/IWSLT.TED.dev2012.de-en.en.xml"
            audio_folder = "/project/iwslt2014/DE/sys2014/data/IWSLT14.SLT.dev2012.de-en.de/dev2012.de-en.de.audio"

            if not(os.path.isdir(data_folder) and os.path.isdir(audio_folder)):
                raise RuntimeError(f"{dataname} not stored locally. Please try another dataset instead.")

            with open(f"{data_folder}/CTM_LIST", 'r') as f:
                srcaudio_ids = f.readlines()
                srcaudio_ids = [x.split("/")[1].split(".")[2].replace("talkid", "") for x in srcaudio_ids]

                srcaudio_paths = []
                for srcaudio_id in srcaudio_ids:
                    for file_path in os.listdir(audio_folder):
                        if file_path.startswith(srcaudio_id):
                            srcaudio_paths.append(f"{audio_folder}/{file_path}")
                            break

            srctext_xml_alllangs = {src_langs: srctext_xml}
            tgttext_xml_alllangs = {tgt_langs: tgttext_xml}
            srcaudio_paths_alllangs = {src_langs: srcaudio_paths}

        else:
            raise RuntimeError(f"Invalid dataname {dataname}")

        # Loading the src text
        srctext_lines_alllangs = {}
        for src_lang in src_langs.split(','):
            srctext_lines_flat = []
            with open(srctext_xml_alllangs[src_lang]) as f:
                xml_str = f.read()
            doc = xml.dom.minidom.parseString(re.sub(r"(<\?xml[^>]+\?>)", r"\1<root>", xml_str) + "</root>")
            docs = doc.getElementsByTagName('doc')
            for doc in docs:
                srctext_lines_per_doc = []
                segs = doc.getElementsByTagName('seg')
                for seg in segs:
                    srctext_lines_per_doc.append(seg.firstChild.nodeValue)
                srctext_lines_flat.append(srctext_lines_per_doc)
            srctext_lines_alllangs[src_lang] = srctext_lines_flat

        # Loading the tgt text
        tgttext_lines_alllangs = {}
        for tgt_lang in tgt_langs.split(','):
            tgttext_lines_flat = []
            with open(tgttext_xml_alllangs[tgt_lang]) as f:
                xml_str = f.read()
            doc = xml.dom.minidom.parseString(re.sub(r"(<\?xml[^>]+\?>)", r"\1<root>", xml_str) + "</root>")
            docs = doc.getElementsByTagName('doc')
            for doc in docs:
                tgttext_lines_per_doc = []
                segs = doc.getElementsByTagName('seg')
                for seg in segs:
                    tgttext_lines_per_doc.append(seg.firstChild.nodeValue)
                tgttext_lines_flat.append(tgttext_lines_per_doc)
            tgttext_lines_alllangs[tgt_lang] = tgttext_lines_flat

    elif re.match(r"LT\.(CS|nonCS)\.2012-..-..", dataname):
        assert len(src_langs.split(',')) == 1 and len(tgt_langs.split(',')) == 1

        data_components = ['srcaudio', 'srctext', 'tgttext']
        cs_or_noncs = dataname.split('.')[1]
        if not (src_langs == 'de' and tgt_langs == 'en'):
            raise RuntimeError(f'Use LT.{cs_or_noncs}.2012 only for de-en')

        data_folder = f"/home/jniehues/data/SLT/de-en/final/LT.{cs_or_noncs}.2012"
        audio_folder = f"{data_folder}/wavs"
        original_srctext_file_path = f"{data_folder}/LT.{cs_or_noncs}.2012.{src_langs}"
        original_tgttext_file_path = f"{data_folder}/LT.{cs_or_noncs}.2012.{tgt_langs}"

        if not (os.path.isdir(data_folder)):
            raise RuntimeError(f"{dataname} not stored locally. Please try another dataset instead.")

        with open(original_srctext_file_path, 'r') as f:
            srctext_lines_flat = f.readlines()
            srctext_lines_flat = [srctext_line.strip() for srctext_line in srctext_lines_flat]

        with open(original_tgttext_file_path, 'r') as f:
            tgttext_lines_flat = f.readlines()
            tgttext_lines_flat = [tgttext_line.strip() for tgttext_line in tgttext_lines_flat]

        with open(f"{data_folder}/FILE_ORDER", 'r') as f:
            srcaudio_names = f.readlines()
            srcaudio_names = [x.strip() for x in srcaudio_names]
            srcaudio_paths = [f"{audio_folder}/{x}" for x in srcaudio_names]

        srctext_lines_alllangs = {src_langs: srctext_lines_flat}
        tgttext_lines_alllangs = {tgt_langs: tgttext_lines_flat}
        srcaudio_paths_alllangs = {src_langs: srcaudio_paths}

    elif 'mTEDx' in dataname or 'tst-COMMON' in dataname:
        data_components = ['srcaudio', 'srctext', 'tgttext']

        assert len(src_langs.split(',')) == 1
        assert len(tgt_langs.split(',')) == 1

        if 'mTEDx' in dataname:
            data_folder = f"{data_root_path}/mtedx/{src_langs}-{tgt_langs}/data/test"
            metadata_path = f'{data_folder}/txt/test.yaml'
            src_text_path = f'{data_folder}/txt/test.{src_langs}'
            tgt_text_path = f'{data_folder}/txt/test.{tgt_langs}'
        else:
            data_folder = "/project/data_asr/iwslt_corpus/eval/MuST-C.tst-COMMON/en-de"
            metadata_path = f'{data_folder}/txt/tst-COMMON.yaml'
            src_text_path = f'{data_folder}/txt/tst-COMMON.{src_langs}'
            tgt_text_path = f'{data_folder}/txt/tst-COMMON.{tgt_langs}'

            if not(os.path.isdir(data_folder)):
                raise RuntimeError(f"{dataname} not stored locally. Please try another dataset instead.")

        # Load text metadata dicts
        with open(metadata_path, 'r') as file:
            text_meta_data_yaml = file.read()
        text_meta_data = yaml.safe_load(text_meta_data_yaml)

        # Load the text sentences
        with open(src_text_path, 'r') as f:
            srctext_lines_flat = f.readlines()
            srctext_lines_flat = [srctext_line.strip() for srctext_line in srctext_lines_flat]
        with open(tgt_text_path, 'r') as f:
            tgttext_lines_flat = f.readlines()
            tgttext_lines_flat = [tgttext_line.strip() for tgttext_line in tgttext_lines_flat]

        df = pd.DataFrame()
        df['srctext_lines'] = srctext_lines_flat
        df['tgttext_lines'] = tgttext_lines_flat
        if 'mTEDx' in dataname:
            df['audio_name'] = [x['wav'].split('.')[0] for x in text_meta_data]
            df['audio_path'] = df['audio_name'].apply(lambda x: f'{data_folder}/wav/{x}.flac')
        else:
            df['audio_name'] = [x['wav'] for x in text_meta_data]
            df['audio_path'] = df['audio_name'].apply(lambda x: f'{data_folder}/wav/{x}')

        srcaudio_paths = df['audio_path'].unique().tolist()
        groups = df.groupby("audio_path", as_index=False)
        srctext_lines = [x['srctext_lines'].tolist() for _, x in groups]
        tgttext_lines = [x['tgttext_lines'].tolist() for _, x in groups]

        srctext_lines_alllangs = {src_langs: srctext_lines}
        tgttext_lines_alllangs = {tgt_langs: tgttext_lines}
        srcaudio_paths_alllangs = {src_langs: srcaudio_paths}

    elif dataname == 'ytseg-test-10-en-en':
        data_components = ['srcaudio', 'srctext', 'chapter']

        test_df = get_partition('test', f"{data_root_path}/ytseg/data")[:10]

        srctext_lines_alllangs = {'en': test_df['text'].tolist()}
        chapter_path_alllangs = {
            'en': test_df.apply(
                lambda x: f"/project/vws/yt_data/{x['channel_id']}/{x['video_id']}.chapters.json", axis=1
            ).tolist()
        }
        srcaudio_paths_alllangs = {'en': test_df['audio_path'].apply(lambda x: f"{data_root_path}/ytseg/" + x).tolist()}

    else:
        raise RuntimeError(f"{dataname} not available")

    # Create folder to store processed data
    os.makedirs(f"{data_root_path}/{processed_data_dir}", exist_ok=True)
    if "IWSLT" in dataname or 'mTEDx' in dataname or 'tst-COMMON' in dataname or 'ytseg' in dataname:
        # For some datasets, also store the text of each audio individually
        if 'srctext' in data_components:
            for src_lang in src_langs.split(','):
                os.makedirs(f"{data_root_path}/{processed_data_dir}/SRCTEXT.{src_lang}", exist_ok=True)
                for i, src_text_per_doc in enumerate(srctext_lines_alllangs[src_lang]):
                    with open(f"{data_root_path}/{processed_data_dir}/SRCTEXT.{src_lang}/SRCTEXT_{i}.txt", 'w') as f:
                        for srctext_line in src_text_per_doc:
                            f.write(f"{srctext_line}\n")
                # Flatten the list
                srctext_lines_alllangs[src_lang] = sum(srctext_lines_alllangs[src_lang], [])

        if 'tgttext' in data_components:
            for tgt_lang in tgt_langs.split(','):
                os.makedirs(f"{data_root_path}/{processed_data_dir}/TGTTEXT.{tgt_lang}", exist_ok=True)
                for i, tgt_text_per_doc in enumerate(tgttext_lines_alllangs[tgt_lang]):
                    with open(f"{data_root_path}/{processed_data_dir}/TGTTEXT.{tgt_lang}/TGTTEXT_{i}.txt", 'w') as f:
                        for tgttext_line in tgt_text_per_doc:
                            f.write(f"{tgttext_line}\n")
                # Flatten the list
                tgttext_lines_alllangs[tgt_lang] = sum(tgttext_lines_alllangs[tgt_lang], [])

        if 'chapter' in data_components:
            for src_lang in src_langs.split(','):
                os.makedirs(f"{data_root_path}/{processed_data_dir}/CHAPTER", exist_ok=True)
                for i, chapter_per_video in enumerate(chapter_path_alllangs[src_lang]):
                    shutil.copyfile(
                        chapter_per_video, f"{data_root_path}/{processed_data_dir}/CHAPTER/CHAPTER_FOR_VIDEO_{i}.json"
                    )
                # Flatten the list
                chapter_path_alllangs[src_lang] = ''.join(chapter_path_alllangs[src_lang])

    # The text from some dataset needs to be detokenized
    if "IWSLT.dev2012" in dataname or re.match(r"LT\.(CS|nonCS)\.2012-..-..", dataname):
        for src_lang in src_langs.split(','):
            srctext_lines_alllangs[src_lang] = [detokenize(tokenized_str=x, lang=src_lang)
                                                for x in srctext_lines_alllangs[src_lang]]
        for tgt_lang in tgt_langs.split(','):
            tgttext_lines_alllangs[tgt_lang] = [detokenize(tokenized_str=x, lang=tgt_lang)
                                                for x in tgttext_lines_alllangs[tgt_lang]]

    # Put the data to an identical format and store to folder
    if 'srctext' in data_components:
        for src_lang in src_langs.split(','):
            # Output to data folder: single file, 1 sentence per line
            os.makedirs(f"{data_root_path}/{processed_data_dir}/SRCTEXT.{src_lang}", exist_ok=True)
            with open(f"{data_root_path}/{processed_data_dir}/SRCTEXT.{src_lang}/SRCTEXT.txt", 'w') as f:
                for srctext_line in srctext_lines_alllangs[src_lang]:
                    f.write(f"{srctext_line}\n")

    if 'tgttext' in data_components:
        for tgt_lang in tgt_langs.split(','):
            # Output to data folder: single file, 1 sentence per line
            os.makedirs(f"{data_root_path}/{processed_data_dir}/TGTTEXT.{tgt_lang}", exist_ok=True)
            with open(f"{data_root_path}/{processed_data_dir}/TGTTEXT.{tgt_lang}/TGTTEXT.txt", 'w') as f:
                for tgttext_line in tgttext_lines_alllangs[tgt_lang]:
                    f.write(f"{tgttext_line}\n")

    if 'srcaudio' in data_components:
        for src_lang in src_langs.split(','):
            # Reformat and output audio to datafolder
            # Order of the audio must match order of the text
            os.makedirs(f"{data_root_path}/{processed_data_dir}/SRCAUDIO.{src_lang}", exist_ok=True)
            if audio_augmentation is None:
                for i, srcaudio_path in enumerate(srcaudio_paths_alllangs[src_lang]):
                    bashCommand = f"ffmpeg -hide_banner -loglevel warning -i {srcaudio_path} -ar 16000 -ac 1 -acodec pcm_s16le " \
                                  f"{data_root_path}/{processed_data_dir}/SRCAUDIO.{src_lang}/audio_{i}.wav"
                    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                    output, error = process.communicate()
            else:
                for i, srcaudio_path in enumerate(srcaudio_paths_alllangs[src_lang]):
                    augment_audio(noise_type=audio_augmentation,
                                  input_audio_path=srcaudio_path,
                                  output_audio_path=f"{data_root_path}/{processed_data_dir}/SRCAUDIO.{src_lang}/audio_{i}.wav"
                                  )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataname",
                        help="Name of the dataset for testing."
                             "Options:"
                             "['newstest2019-en-de', 'covost2-test-en-de', 'IWSLT<xxxx>-test-en-de', "
                             "# E.g., IWSLT2019-test-en-de"
                             "'IWSLT.ACLdev2023-en-de', 'IWSLT.dev2012-de-en' 'LT.CS.2012-de-en' 'LT.nonCS.2012-de-en',"
                             "'mTEDx-fr-en', 'ytseg-test-10-en-en']"
                             ,
                        type=str)
    parser.add_argument("--data_root_path",
                        help="Path to the folder that stores all data", type=str, default=None)
    parser.add_argument("--processed_data_dir",
                        help="Directory name to store the processed data", type=str, default=None)
    parser.add_argument("--audio_augmentation",
                        choices=[None, 'echo', 'noise', 'echonoise'], type=none_or_str, default=None)
    parser.add_argument("--src_langs",
                        help="In theory can be multiple, separated by comma. E.g., 'de,fr,ja'. Currently only single",
                        type=str, default=None)
    parser.add_argument("--tgt_langs",
                        help="Can be multiple, separated by comma. E.g., 'de,fr,ja'."
                             "Currently only multiple for ACL",
                        type=str, default=None)
    args = parser.parse_args()

    download_data(dataname=args.dataname, data_root_path=args.data_root_path,
                  src_langs=args.src_langs, tgt_langs=args.tgt_langs)
    process_data(dataname=args.dataname, data_root_path=args.data_root_path,
                 src_langs=args.src_langs, tgt_langs=args.tgt_langs,
                 processed_data_dir=args.processed_data_dir,
                 audio_augmentation=args.audio_augmentation)

