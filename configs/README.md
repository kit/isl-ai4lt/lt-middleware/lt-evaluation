NOTE: no inline comment in the config files, as the files is later on passed into python script
An example config.sh file is as follows:

```
dataname=covost2-test-en-de
# task: [MT, ST]
# If MT is selected, it is evaluated independently with gold standard input
# If (cascaded) ST is selected, ASR is evaluated along side.
task=ST
segmenter=VAD
ffmpeg_speed=-1
version=offline
stability_detection=False
ASRmode=SendStable
MTmode=SendStable
use_prep=True
```
