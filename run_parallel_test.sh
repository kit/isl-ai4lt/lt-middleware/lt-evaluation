#!/bin/bash

server_url=$1

declare -a nr_runs=( 1 2 5 10 )

for nr_run in ${nr_runs[@]}; do
  # Run nr_run times in parallel
  for ((i=1; i<=${nr_run}; i++)); do
    . run_evaluate_pipeline_different_settings.sh ${server_url} "${nr_run}_parallel" & echo "start run ${i}th"
  done
  wait
done