from pathlib import Path
import json
import unicodedata
import re
from dataclasses import dataclass
import copy
import ast

import argparse

def fmt_text(text):
    _RE_COMBINE_WHITESPACE = re.compile(r"\s+")
    return _RE_COMBINE_WHITESPACE.sub(" ", unicodedata.normalize("NFKD", text)).strip()


def sent_tokenize(text):
    punctuation = [".", "?", "!", "。"]
    # Create a regular expression pattern that matches any of the punctuation marks followed by a space or end of string
    pattern = '|'.join(re.escape(p) for p in punctuation)
    # Split the text using the pattern
    sentences = re.split(f'(?<={pattern})', text)
    # Remove any leading/trailing whitespace from each sentence and filter out any empty strings
    sentences = [sentence.strip() for sentence in sentences if sentence.strip()]
    return sentences


def filter_captions(captions):
    _captions = []
    for caption in captions:
        if fmt_text(caption['text']) == "" or caption['unstable']:
            continue
        _captions.append(caption)
    return _captions

def get_sentences(captions):
    return sent_tokenize(' '.join([fmt_text(c['text']) for c in captions]))

def join_captions(captions):
    return ' '.join([fmt_text(c.text) for c in captions])

def check_captions(captions, sentence):
    current_caption_idx = 1
    current_caption = join_captions(captions[0:current_caption_idx+1])

    for _ in range(len(captions)):
        if sentence == current_caption:
            return (True, current_caption_idx, current_caption)
        elif current_caption.startswith(sentence):
            return (False, current_caption_idx, current_caption)
        current_caption_idx += 1
        current_caption = join_captions(captions[0:current_caption_idx+1])

@dataclass
class Caption:
    start: float
    end: float
    text: str

def get_annotated_sentences(captions, sentences):
    annotated_sentences = []

    for sentence in sentences:

        caption = captions[0]
        caption_text = fmt_text(caption.text)
        start = caption.start
        end = caption.end
        duration = end - start

        if sentence == caption_text:
            annotated_sentences.append({
                'text': sentence,
                'start': start,
                'end': end,
            })
            captions.pop(0)
        elif sentence.startswith(caption_text):
            is_exact_match, caption_idx, current_caption = check_captions(captions, sentence)
            if is_exact_match:
                annotated_sentences.append({
                    'text': sentence,
                    'start': start,
                    'end': captions[caption_idx].end,
                })
                captions = captions[caption_idx+1:]
            else:
                current_caption_start = captions[caption_idx].start
                current_caption_duration = captions[caption_idx].end - captions[caption_idx].start
                current_caption_text = current_caption[len(sentence):]
                duration_ratio = 1 - len(current_caption_text) / len(captions[caption_idx].text)

                interpolated_sentence_end = current_caption_start + current_caption_duration * duration_ratio
                annotated_sentences.append({
                    'text': sentence,
                    'start': start,
                    'end': interpolated_sentence_end,
                })
                captions = captions[caption_idx:]

                captions[0].text = current_caption[len(sentence):].lstrip(" ")
                captions[0].start = interpolated_sentence_end
        elif caption_text.startswith(sentence):
            interpolated_sentence_end = start + duration * len(sentence) / len(caption_text)

            annotated_sentences.append({
                'text': sentence,
                'start': start,
                'end': interpolated_sentence_end,
            })

            caption.text = caption_text[len(sentence):].lstrip(" ")
            caption.start = interpolated_sentence_end
        else:
            break
    return captions, annotated_sentences

def get_sections(chapters, annotated_sentences):
    sections = []
    current_section = []
    temp_chapters = copy.deepcopy(chapters)

    for s in annotated_sentences:
        sentence_start = s['start']
        sentence_end = s['end']
        chapter_start = temp_chapters[0]['start_time']
        chapter_end = temp_chapters[0]['end_time']

        is_inside = sentence_end <= chapter_end and sentence_start >= chapter_start
        is_before = sentence_end < chapter_start and sentence_start < chapter_start
        is_after = sentence_end > chapter_end and sentence_start > chapter_end

        span_before = sentence_start < chapter_start and sentence_end <= chapter_end
        span_after = sentence_end > chapter_end and sentence_start >= chapter_start

        # if temp_chapters:
        #     print(s['end'], temp_chapters[0]['start_time'])
        if not temp_chapters:
            # print("No more chapters")
            current_section.append(s)

        if is_inside:
            # print("Is Inside: Unambiguous")
            current_section.append(s)
        elif is_after:
            # print("Is after: Unambiguous")
            sections.append(current_section)
            current_section = [s]
            temp_chapters.pop(0)
        elif is_before:
            raise Exception("Is before: Should not happen")
        elif span_before:
            raise Exception("Span before: Should not happen")
        elif span_after:
            if len(temp_chapters) == 1:
                current_section.append(s)
                continue

            next_chapter_start = temp_chapters[1]['start_time']

            # check if more than half of the sentence is in the chapter
            if (chapter_end - sentence_start) > (sentence_end - next_chapter_start):
                # print("-- Add to current section")
                current_section.append(s)
            else:
                # print("-- Add to next section")
                sections.append(current_section)
                current_section = [s]
                temp_chapters.pop(0)
        else:
            raise Exception("Should not happen")
    sections.append(current_section)
    return sections

def add_edge_chapters(chapters, captions):
    if chapters[0]['start_time'] > captions[0].start:
        chapters.insert(0, {'start_time': captions[0].start, 'end_time': chapters[0]['start_time'], 'title': 'Intro', 'generated': True})

    if chapters[-1]['end_time'] < captions[-1].end:
        chapters.append({'start_time': chapters[-1]['end_time'], 'end_time': captions[-1].end, 'title': 'Outro', 'generated': True})

def prune_empty_generated_edge_chapters(sections, chapters):
    if len(chapters) == len(sections)+1:
        sections.append([])
    def prune_empty_generated_chapter(idx):
        if len(sections[idx]) == 0:
            if 'generated' in chapters[idx] and chapters[idx]['generated']:
                chapters.pop(idx)
                sections.pop(idx)
    prune_empty_generated_chapter(0)
    prune_empty_generated_chapter(-1)

def check_asserts(annotated_sentences, _captions, sentences):
    # all sentences should be annotated
    assert len(annotated_sentences) == len(sentences)
    assert len(_captions) == 0

    # combined annotated sentences should be equal to original sentences
    assert ' '.join([s['text'] for s in annotated_sentences]) == ' '.join(sentences)

    # interpolated timestamps should be correct
    assert annotated_sentences[0]['start'] == captions[0].start
    assert annotated_sentences[-1]['end'] == captions[-1].end

def fix_inconsistent_timestamps(captions):
    if captions[0].start > captions[0].end:
        time_set = sorted({captions[0].start, captions[1].start, captions[0].end, captions[1].end})
        assert len(time_set) == 3
        captions[0].start, captions[0].end = time_set[:2]
        captions[1].start, captions[1].end = time_set[1:3]

    if captions[-1].end < captions[-1].start:
        time_set = sorted({captions[-2].start, captions[-1].start, captions[-2].end, captions[-1].end})
        assert len(time_set) == 3
        captions[-2].start, captions[-2].end = time_set[:2]
        captions[-1].start, captions[-1].end = time_set[1:3]

    for i, caption in enumerate(captions):
        if caption.start > caption.end:
            time_set = sorted({captions[i-1].start, captions[i].start, captions[i+1].start, captions[i-1].end, captions[i].end, captions[i+1].end})
            assert len(time_set) == 4
            captions[i-1].start, captions[i-1].end = time_set[:2]
            captions[i].start, captions[i].end = time_set[1:3]
            captions[i+1].start, captions[i+1].end = time_set[2:4]

# from Wiki Topic Segmentation
def deep_len(l):
    lengths = [len(ll) for ll in l]
    return sum(lengths), lengths

def get_target(sections):
    length, section_lengths = deep_len(sections)
    target_sequence = "".join(map(lambda x: '1' + '0'*(x-1), section_lengths))
    target_sequence = '0' + target_sequence[1:]
    return target_sequence


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--transcript_timestamp_path", type=str, required=True)
    parser.add_argument("--chapter_path", type=str, required=True)
    parser.add_argument("--output_target_path", type=str, required=True)

    args = parser.parse_args()

    sections_list = []
    targets_list = []
    channel_list = []
    chapters_list = []
    video_list = []
    annotated_sentences_list = []

    i = 0
    j = 0

    with open(f"{args.transcript_timestamp_path}", 'r') as f:
        captions = f.readlines()
        captions = [ast.literal_eval(caption.strip()) for caption in captions]

    with open(args.chapter_path, 'r') as f:
        chapters = json.load(f)


    captions = filter_captions(captions)
    sentences = get_sentences(captions)
    captions = [Caption(c['audio_start'], c['audio_end'], c['text']) for c in captions]

    if len(captions) == 0:
        print("No captions")
        exit()

    try:
        fix_inconsistent_timestamps(captions)
    except (AssertionError, IndexError):
        print("Failed fixing timestamps")

    if not all(c.start <= c.end for c in captions):
        print("Inconsistent time stamps")
        exit()


    _captions, annotated_sentences = get_annotated_sentences(copy.deepcopy(captions), sentences)
    check_asserts(annotated_sentences, _captions, sentences)

    add_edge_chapters(chapters, captions)
    sections = get_sections(chapters, annotated_sentences)


    prune_empty_generated_edge_chapters(sections, chapters)
    if len(sections) < len(chapters):
            print("Inconsistent chapter numbers")
            exit()
    elif len(chapters) > len(sections):
            print("Unexpected")
            exit()
    target = get_target(sections)

    chapters = [chapter["title"] for chapter in chapters]

    sections_ = [[sentence['text'] for sentence in section] for section in sections]
    sections_ = [sentence for section in sections_ for sentence in section] # flatten sections

    with open(args.output_target_path, 'w') as f:
        f.write(target)

