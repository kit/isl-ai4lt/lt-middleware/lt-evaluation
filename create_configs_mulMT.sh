#!/bin/bash

if [ -z "$1" ]; then
  configs_path=configs
else
  configs_path=$1
fi

# Store different settings to seperate files in configs/ folder
mkdir -p ${configs_path}
rm ${configs_path}/*.sh
declare -a datanames=( "IWSLT.ACLdev2023-en-de" "IWSLT.ACLdev2023-en-de,ar" "IWSLT.ACLdev2023-en-de,ar,nl,fr,fa" )

use_prep="False"
minPrefixExtension=2
LA2_chunk_size=2
nr_streaming_mt=5

config_counter=0
for dataname in ${datanames[@]}; do
  src_lang=$(echo ${dataname} | awk -F'-' '{print $(NF-1)}')
  if [[ ${src_lang} == "en" ]]; then
      asr_server="ASR_denoise_iso"
      mt_server="mdeltaLM_iso"
  else
      asr_server="ASR_quan_nonla_iso"
      mt_server="MT_quan_nonla_iso"
  fi

  # Online settings
  declare -a stability_detections=( "True" )
  declare -a resendings=( "True" )
  for stability_detection in ${stability_detections[@]}; do
    for resending in ${resendings[@]}; do
      if [[ $resending == "True" ]]; then
        ASRmode="SendUnstable"
        MTmode="SendUnstable"
      else
        ASRmode="SendStable"
        MTmode="SendPartial"
      fi
      echo "dataname=${dataname}
task=cascadedST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
MTmode=${MTmode}
use_prep=${use_prep}
asr_server=${asr_server}
mt_server=${mt_server}
resending=${resending}
minPrefixExtension=${minPrefixExtension}
LA2_chunk_size=${LA2_chunk_size}
nr_streaming_mt=${nr_streaming_mt}" >> ${configs_path}/config${config_counter}.sh
      config_counter=$((config_counter+1))
    done
  done
done