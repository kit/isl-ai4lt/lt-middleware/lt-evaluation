#!/bin/bash

if [ -z "$1" ]; then
  configs_path=configs
else
  configs_path=$1
fi

# Store different settings to seperate files in configs/ folder
mkdir -p ${configs_path}
rm ${configs_path}/*.sh
declare -a datanames=( "IWSLT.ACLdev2023-en-de" "IWSLT2019-test-en-de" )

declare -a LA2_chunk_sizes=( 0.5 1 2 3 )
use_prep="False"
stability_detection="True"

config_counter=0
for dataname in ${datanames[@]}; do
  # Evaluate cascaded ST
  asr_server="ASR_denoise_iso"
  mt_server="mdeltaLM_iso"
  minPrefixExtension=2

  declare -a resendings=( "True" "False" )
  for resending in ${resendings[@]}; do
    for LA2_chunk_size in ${LA2_chunk_sizes[@]}; do
        if [[ $resending == "True" ]]; then
          ASRmode="SendUnstable"
          MTmode="SendUnstable"
        else
          ASRmode="SendStable"
          MTmode="SendPartial"
        fi
        echo "dataname=${dataname}
task=cascadedST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
MTmode=${MTmode}
use_prep=${use_prep}
asr_server=${asr_server}
mt_server=${mt_server}
resending=${resending}
minPrefixExtension=${minPrefixExtension}
LA2_chunk_size=${LA2_chunk_size}" >> ${configs_path}/config${config_counter}.sh
        config_counter=$((config_counter+1))
    done
  done


  # Evaluate e2e ST
  asr_server="e2e_en2de_iso"
  declare -a resendings=("False" )

  for LA2_chunk_size in ${LA2_chunk_sizes[@]}; do
    for resending in ${resendings[@]}; do
      if [[ $resending == "True" ]]; then
        ASRmode="SendUnstable"
      else
        ASRmode="SendStable"
      fi
      echo "dataname=${dataname}
task=e2eST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
use_prep=${use_prep}
resending=${resending}
asr_server=${asr_server}
LA2_chunk_size=${LA2_chunk_size}" >> ${configs_path}/config${config_counter}.sh
      config_counter=$((config_counter+1))
    done
  done


done