from cachetools import cached
import torchmetrics
import segeval
# from nltk.metrics.segmentation import ghd
from collections import defaultdict
import numpy as np


@cached(cache={})
def get_eval_torchmetrics(threshold=0.5):
    return {
        'accuracy': torchmetrics.Accuracy(task='binary', num_classes=1, threshold=threshold),
        'precision': torchmetrics.Precision(task='binary', num_classes=1, threshold=threshold),
        'recall': torchmetrics.Recall(task='binary', num_classes=1, threshold=threshold),
        'specificity': torchmetrics.Specificity(task='binary', num_classes=1, threshold=threshold),
        'f1': torchmetrics.F1Score(task='binary', num_classes=1, threshold=threshold),
    }


eval_segeval_metrics = {
    'pk': segeval.pk,
    'window_diff': segeval.window_diff,
    'boundary_similarity': segeval.boundary_similarity,
}

# eval_nltk_metrics = {
#     'ghd': ghd,
# }

eval_data_stats = {
    'num_segments': lambda x: x.count('1'),
    'num_sentences': lambda x: len(x),
}

masses = segeval.convert_nltk_to_masses


def convert_to_classes(output, threshold=0.5):
    return (output > threshold).astype(float)


def calc_metrics(output, encoded_targets, targets, lengths, prefix='', average=True, threshold=0.5):
    eval_torchmetrics = get_eval_torchmetrics(threshold=threshold)

    batch_metrics = defaultdict(list)
    for l, o, t, et in zip(lengths, output, targets, encoded_targets):
        length = int(l.item())
        o_ = o.float()
        o_l = o_[:length]
        et_l = et.int()[:length]

        for metric_name, metric_fn in eval_torchmetrics.items():
            metric_value = metric_fn(o_l, et_l)
            batch_metrics[metric_name].append(metric_value.item())

        o_s = ''.join(
            str(int(x)) for x in convert_to_classes(o_l.squeeze().detach().cpu().numpy(), threshold=threshold))
        o_s_masses, t_masses = masses(o_s), masses(t)
        for metric_name, metric_fn in eval_segeval_metrics.items():
            try:
                metric_value = metric_fn(o_s_masses, t_masses)
            except Exception as e:
                if o_s == t:
                    metric_value = 1
                else:
                    raise e
            batch_metrics[metric_name].append(float(metric_value))

        # for metric_name, metric_fn in eval_nltk_metrics.items():
        #     metric_value = metric_fn(o_s, t)
        #     batch_metrics[metric_name].append(metric_value)

        batch_metrics['num_segments'].append(eval_data_stats['num_segments'](o_s))
        batch_metrics['reference_num_segments'].append(eval_data_stats['num_segments'](t))
        batch_metrics['reference_num_sentences'].append(eval_data_stats['num_sentences'](t))

    batch_metrics = {((prefix if not 'reference' in k else '') + k): (np.average(v) if average else v) for k, v in
                     batch_metrics.items()}

    return batch_metrics
