#!/bin/bash

if [ -z "$1" ]; then
  configs_path=configs
else
  configs_path=$1
fi

# Store different settings to seperate files in configs/ folder
mkdir -p ${configs_path}
rm -rf ${configs_path}/*.sh
declare -a datanames=( "IWSLT.ACLdev2023-en-de" )
declare -a stability_detections=("True" )
declare -a resendings=("True" )

use_prep="False"
asr_server="e2e_en2de_iso"
LA2_chunk_size=2
nr_streaming_asr=2

config_counter=0
for dataname in ${datanames[@]}; do
  # Evaluate e2eST
  # Online settings
  for stability_detection in ${stability_detections[@]}; do
    for resending in ${resendings[@]}; do
      if [[ $resending == "True" ]]; then
        ASRmode="SendUnstable"
      else
        ASRmode="SendStable"
      fi
      echo "dataname=${dataname}
task=e2eST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
use_prep=${use_prep}
resending=${resending}
asr_server=${asr_server}
LA2_chunk_size=${LA2_chunk_size}
nr_streaming_asr=${nr_streaming_asr}" >> ${configs_path}/config${config_counter}.sh
  config_counter=$((config_counter+1))
    done
  done
done