#!/bin/bash

if [ -z "$1" ]; then
  configs_path=configs
else
  configs_path=$1
fi

# Store different settings to seperate files in configs/ folder
mkdir -p ${configs_path}
rm ${configs_path}/*.sh
declare -a datanames=( "IWSLT.ACLdev2023-en-de" "IWSLT2019-test-en-de" "IWSLT2020-test-en-de" "IWSLT.ACLdev2023-en-ja" "IWSLT.ACLdev2023-en-zh" "IWSLT.ACLdev2023-en-ar" "IWSLT.ACLdev2023-en-nl" "IWSLT.ACLdev2023-en-fr" "IWSLT.ACLdev2023-en-fa" "IWSLT.ACLdev2023-en-pt" "IWSLT.ACLdev2023-en-ru" "IWSLT.ACLdev2023-en-tr" "mTEDx-es-en" "mTEDx-fr-en" "mTEDx-pt-en" "mTEDx-it-en" "LT.nonCS.2012-de-en" "LT.CS.2012-de-en" )

use_prep="False"
minPrefixExtension=2
LA2_chunk_size=2

config_counter=0
for dataname in ${datanames[@]}; do
  src_lang=$(echo ${dataname} | awk -F'-' '{print $(NF-1)}')
  if [[ ${src_lang} == "en" ]]; then
      asr_server="ASR_denoise_iso"
      mt_server="mdeltaLM_iso"
  else
      asr_server="ASR_quan_nonla_iso"
      mt_server="MT_quan_nonla_iso"
  fi


  # Evaluate cascaded ST
  # Offline settings
  declare -a segmenters=("VAD" "SHAS" )
  for segmenter in ${segmenters[@]}; do
  echo "dataname=${dataname}
task=cascadedST
segmenter=${segmenter}
ffmpeg_speed=-1
version=offline
stability_detection=False
ASRmode=SendStable
MTmode=SendStable
use_prep=${use_prep}
asr_server=${asr_server}
mt_server=${mt_server}" >> ${configs_path}/config${config_counter}.sh
    config_counter=$((config_counter+1))
  done

  # Online settings
  declare -a stability_detections=( "True" )
  declare -a resendings=("True" "False" )
  for stability_detection in ${stability_detections[@]}; do
    for resending in ${resendings[@]}; do
      if [[ $resending == "True" ]]; then
        ASRmode="SendUnstable"
        MTmode="SendUnstable"
      else
        ASRmode="SendStable"
        MTmode="SendPartial"
      fi
      echo "dataname=${dataname}
task=cascadedST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
MTmode=${MTmode}
use_prep=${use_prep}
asr_server=${asr_server}
mt_server=${mt_server}
resending=${resending}
minPrefixExtension=${minPrefixExtension}
LA2_chunk_size=${LA2_chunk_size}" >> ${configs_path}/config${config_counter}.sh
      config_counter=$((config_counter+1))
    done
  done




  # Evaluate e2eST
  # Offline settings
  asr_server="e2e_en2de_iso"
  declare -a segmenters=("VAD" "SHAS" )
  for segmenter in ${segmenters[@]}; do
    echo "dataname=${dataname}
task=e2eST
segmenter=${segmenter}
ffmpeg_speed=-1
version=offline
stability_detection=False
ASRmode=SendStable
asr_server=${asr_server}
use_prep=${use_prep}" >> ${configs_path}/config${config_counter}.sh
    config_counter=$((config_counter+1))
  done

  # Online settings
  declare -a stability_detections=("True" )
  declare -a resendings=("True" "False" )
  for stability_detection in ${stability_detections[@]}; do
    for resending in ${resendings[@]}; do
      if [[ $resending == "True" ]]; then
        ASRmode="SendUnstable"
      else
        ASRmode="SendStable"
      fi
      echo "dataname=${dataname}
task=e2eST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
use_prep=${use_prep}
resending=${resending}
asr_server=${asr_server}
LA2_chunk_size=${LA2_chunk_size}" >> ${configs_path}/config${config_counter}.sh
  config_counter=$((config_counter+1))
    done
  done
done