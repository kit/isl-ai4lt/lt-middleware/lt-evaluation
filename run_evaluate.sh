#!/bin/bash

ref_path=$1
raw_output_path=$2
sentences_output_path=$3
timestamps_output_path=$4
resegment_sentences_output_path=$5
score_output_path=$6
task=$7
src_lang=$8
tgt_lang=$9
audio_index=${10}
chapter_path=${11}
segment_target_path=${12}
segment_sysout_path=${13}

if [[ "$task" == "MT" ]]; then
  metrics=( "BLEU" )
elif [[ "$task" == "e2eST" || "$task" == "cascadedST" ]]; then
  metrics=( "BLEU" "latency_ST" "avgStablizeTime_ST" "latencySysInd_ST" "avgFlickers_ST" )
elif [[ "$task" == ASR* ]]; then
  metrics=( "WER" "latency_ASR" "avgStablizeTime_ASR" "latencySysInd_ASR" "avgFlickers_ASR" )
fi

if [[ "$task" == *Chaptering* ]]; then
  metrics+=( "Chaptering" )
fi

# Download mwerSegmenter if it's not yet there
SCRIPTDIR="mwerSegmenter"
if [ ! -d "${SCRIPTDIR}" ]; then
  echo "Downloading mwerSegmenter"
  wget --no-check-certificate https://www-i6.informatik.rwth-aachen.de/web/Software/mwerSegmenter.tar.gz
  tar -zxvf mwerSegmenter.tar.gz
fi

# Extract the output sentences from the raw json output
# If ja,zh,ko then also tokenize the sentences
python ExtractingOutput.py \
  --raw_output ${raw_output_path} \
  --sentences_output ${sentences_output_path} \
  --timestamps_output ${timestamps_output_path} \
  --segment_output ${segment_sysout_path} \
  --raw_output_type ${task} \
  --src_lang ${src_lang} \
  --tgt_lang ${tgt_lang}

# Evaluate chaptering
if [[ ( $task == "ASRChaptering" ) && ( $chapter_path != "none" ) && ( $segment_target_path != "none" ) ]]; then
  python generate_chapter_targets.py \
    --transcript_timestamp_path ${timestamps_output_path} \
    --chapter_path ${chapter_path} \
    --output_target_path ${segment_target_path}
fi


# Resegment based on the reference
# If ja,zh,ko then have to first tokenize the ref
ref_path_tok="tokenized_ref.txt"
if [[ $tgt_lang == "ja" ]]; then
  python tokenization.py --input_file ${ref_path} --output_file ${ref_path_tok} --lang $tgt_lang
  ref_path=${ref_path_tok}
elif [[ $tgt_lang == "zh" ]]; then
  python tokenization.py --input_file ${ref_path} --output_file ${ref_path_tok} --lang $tgt_lang
  ref_path=${ref_path_tok}
elif [[ $tgt_lang == "ko" ]]; then
  python tokenization.py --input_file ${ref_path} --output_file ${ref_path_tok} --lang $tgt_lang
  ref_path=${ref_path_tok}
fi

$SCRIPTDIR/mwerSegmenter -mref ${ref_path} -hypfile ${sentences_output_path}
if [[ -f "__segments" ]]; then
  mv "__segments" ${resegment_sentences_output_path}
else
  echo "***** mwerSegmenter failed. Using sacrebleu segmenter instead. ******"
  python3 -m sacrebleu -l ${src_lang}-${tgt_lang} \
    ${ref_path} \
    --input ${sentences_output_path} \
    --resegment \
    --output_resegment ${resegment_sentences_output_path}
fi

# Resegment based on the reference, but both lowercased
add_suffix_to_file() {
  file_path=$1
  suffix=$2
  dir_path=$(dirname "$file_path")
  file_name=$(basename -- "$file_path")
  file_name_without_ext="${file_name%.*}"
  new_file_name="${file_name_without_ext}${suffix}.${file_path##*.}"
  new_file_path="$dir_path/$new_file_name"
  echo "$new_file_path"
}
lc_resegment_sentences_output_path=$(add_suffix_to_file $resegment_sentences_output_path "_lower")
$SCRIPTDIR/mwerSegmenter -mref <(awk '{print tolower($0)}' "$ref_path") -hypfile <(awk '{print tolower($0)}' "$sentences_output_path")
if [[ -f "__segments" ]]; then
  mv "__segments" ${lc_resegment_sentences_output_path}
else
  echo "***** mwerSegmenter failed. Using sacrebleu segmenter instead. ******"
  python3 -m sacrebleu -l ${src_lang}-${tgt_lang} \
    <(awk '{print tolower($0)}' "$ref_path") \
    --input <(awk '{print tolower($0)}' "$sentences_output_path") \
    --resegment \
    --output_resegment ${lc_resegment_sentences_output_path}
fi

# Evaluate
python Evaluation.py \
  --sentences-output-path ${resegment_sentences_output_path} \
  --lowercased-sentences-output-path ${lc_resegment_sentences_output_path} \
  --timestamps-output-path ${timestamps_output_path} \
  --ref-path ${ref_path} \
  --segment-target-path ${segment_target_path} \
  --segment-sysout-path ${segment_sysout_path} \
  --metrics ${metrics[@]} \
  --audio_index ${audio_index} \
  --output_lang ${tgt_lang} \
  | tee -a ${score_output_path}

rm -rf ${ref_path_tok}