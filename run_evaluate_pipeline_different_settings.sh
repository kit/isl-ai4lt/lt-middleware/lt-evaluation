#!/bin/bash

server_url=$1

if [ -z "$2" ]; then
  experiment_name=all_configs
else
  experiment_name=$2
fi

ARTIFACT_PATH=${ARTIFACT_PATH:-$(pwd)}
configs_root_dir="${ARTIFACT_PATH}/configs"
#configs_path="${ARTIFACT_PATH}/configs_${experiment_name}"
configs_path=$(mktemp --tmpdir="$configs_root_dir" -d)

if [[ ${experiment_name} == *"parallel"* ]]; then
  config_creation_file="create_configs_parallel.sh"
else
  config_creation_file="create_configs_${experiment_name}.sh"
fi

if [[ -f ${config_creation_file} ]]; then
  . ${config_creation_file} ${configs_path}
fi

# Count the number of configs
nr_configs=$(ls ${configs_path}/config*.sh | wc -l )
# Run each config one by one in the correct order
for ((i=0;i<${nr_configs};i++));
do
  config_file=${configs_path}/config${i}.sh
  # Run evaluation with each config file in the configs folder and log to mlflow
  echo ${config_file}
  bash run_evaluate_pipeline.sh $config_file $experiment_name $server_url
done

# Remove the temporary configs directory
rm -rf "$configs_path"