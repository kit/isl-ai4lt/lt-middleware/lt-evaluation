#!/usr/bin/env python3
import argparse
import jiwer
import numpy as np
import sacrebleu
import ast
import os
from sacrebleu.tokenizers import TOKENIZERS
from chaptering_metrics import calc_metrics
import torch


def none_or_int(value):
    if value.lower() == 'none':
        return None
    return int(value)


def none_or_str(value):
    if value.lower() == 'none':
        return None
    return str(value)


def calculate_bleu(trans: list, refs: list, output_lang: str) -> float:
    if output_lang in ['zh', 'ja', 'ko']:
        # The hypothesis and ref is already tokenized before (in order for mwerSegmenter to work)
        return sacrebleu.corpus_bleu(trans, [refs], tokenize='none').score  # Tell sacrebleu to do no tokenization now
    else:
        return sacrebleu.corpus_bleu(trans, [refs]).score


def calculate_wer(sys_outputs: list, refs: list) -> float:
    return jiwer.wer(refs, sys_outputs)


def calculate_wer_without_case_punct(sys_outputs: list, refs: list) -> float:
    transformation = jiwer.Compose([
        jiwer.ToLowerCase(),
        jiwer.RemovePunctuation(),
        jiwer.RemoveMultipleSpaces(),
        jiwer.Strip(),
        jiwer.ReduceToListOfListOfWords(),
    ])
    return jiwer.wer(refs, sys_outputs, truth_transform=transformation, hypothesis_transform=transformation)

def timestamps_to_website_output(timestamps):
    data = []
    stable = []
    stable_timestamps = []
    unstable = []
    i = 0
    while i < len(timestamps):
        t1 = timestamps[i]
        start = t1["audio_start"]
        end = t1["audio_end"]
        received = t1["output_received"]
        if t1["unstable"]: # unstable
            unstable = t1["text"].strip().split()
            if len(unstable) > 0:
                unstable_timestamps = [start + j*(end-start)/len(unstable) for j in range(len(unstable)+1)]
            else:
                unstable_timestamps = []
            if len(stable_timestamps) != 0:
                unstable_timestamps = unstable_timestamps[1:]
        else: # stable
            if i < len(timestamps) - 1:
                t2 = timestamps[i+1]
                if t2["output_received"] - t1["output_received"] < 0.1:
                    received = t2["output_received"]
                    words = t1["text"].strip().split()
                    stable += words
                    if len(stable_timestamps) == 0:
                        stable_timestamps.append(start)
                    if len(words) > 0:
                        stable_timestamps += [start + j*(end-start)/len(words) for j in range(len(words)+1)][1:]
                    unstable = t2["text"].strip().split()
                    start2 = t2["audio_start"]
                    end2 = t2["audio_end"]
                    unstable_timestamps = [start2 + (j+1)*(end2-start2)/len(unstable) for j in range(len(unstable))]
                    i += 1
                else:
                    words = t1["text"].strip().split()
                    stable += words
                    if len(stable_timestamps) == 0:
                        stable_timestamps.append(start)
                    if len(words) > 0:
                        stable_timestamps += [start + (j+1)*(end-start)/len(words) for j in range(len(words))]
                    unstable = []
                    unstable_timestamps = []
            else:
                words = t1["text"].strip().split()
                stable += words
                if len(stable_timestamps) == 0:
                    stable_timestamps.append(start)
                if len(words) > 0:
                    stable_timestamps += [start + (j+1)*(end-start)/len(words) for j in range(len(words))]
                unstable = []
                unstable_timestamps = []

        d = {"timestamps":stable_timestamps+unstable_timestamps, "received":received, "text": stable+unstable}
        data.append(d)

        if len(d["timestamps"])-1 != len(d["text"]):
            print("NOOOO")
            breakpoint()

        i += 1

    return data

def calculate_latency(timestamps):
    """
    Use the formula in
    https://www.researchgate.net/profile/Thanh-Le-Ha/publication/307889273_Dynamic_Transcription_for_Low-Latency_Speech_Translation/links/5d724594a6fdcc9961b219bc/Dynamic-Transcription-for-Low-Latency-Speech-Translation.pdf

    :param timestamps: the timestamp of the start of audio segnment, end of audio segment,
    received back translation/transcription, whether the translation/transcription is stable.
    From a single session

    :return:
        stable_latency:  Avg time required until the final stable output is returned (s)
        unstable_latency: Avg time required until the first unstable output is returned (s)
        first_unchange_latency: Avg time required until the first non-changing output is returned (s)
    """
    stable_latency_num = 0
    stable_latency_den = 0
    unstable_latency_num = 0
    unstable_latency_den = 0
    audio_pointer = 0

    for timestamp in timestamps:
        # Calculate stable latency
        if not timestamp['unstable']:
            delay = timestamp['output_received'] - \
                    ((timestamp['audio_start'] + timestamp['audio_end']) / 2)
            stable_latency_num += delay * (timestamp['audio_start'] - timestamp['audio_end'])
            stable_latency_den += timestamp['audio_start'] - timestamp['audio_end']
            #print("%.2f"%timestamp['audio_start'],"%.2f"%timestamp['audio_end'],timestamp["text"])

        # Calculate unstable latency
        if audio_pointer <= timestamp['audio_start']:
            delay = timestamp['output_received'] - \
                    ((timestamp['audio_start'] + timestamp['audio_end']) / 2)
            unstable_latency_num += delay * (timestamp['audio_start'] - timestamp['audio_end'])
            unstable_latency_den += timestamp['audio_start'] - timestamp['audio_end']
            audio_pointer = timestamp['audio_end']
        elif audio_pointer < timestamp['audio_end']:
            delay = timestamp['output_received'] - \
                    ((audio_pointer + timestamp['audio_end']) / 2)
            unstable_latency_num += delay * (audio_pointer - timestamp['audio_end'])
            unstable_latency_den += audio_pointer - timestamp['audio_end']
            audio_pointer = timestamp['audio_end']

    data = timestamps_to_website_output(timestamps)

    #for d in data:
    #    print(" ".join(d["text"]))
    #    print(d['timestamps'])

    # Calculate first_unchange_latency
    unchange_latency_num = 0
    unchange_latency_den = 0

    i = len(data)-1
    j_last = len(data[-1]["text"]) + 1000
    while i > 0:
        d2 = data[i]
        d1 = data[i-1]

        te1 = d1["text"][:j_last]
        te2 = d2["text"][:j_last]
        ts2 = d2["timestamps"][:j_last+1]
        r = d2["received"]

        if te1 != te2:
            #print(te1)
            #print(te2)

            for j,(w1,w2,t1) in enumerate(zip(te1,te2,ts2)):
                if w1!=w2:
                    t2 = ts2[-1]
                    delay = r-(t2+t1)/2
                    unchange_latency_num += delay * (t2 - t1)
                    unchange_latency_den += t2 - t1
                    #print("%.2f"%t1,"%.2f"%t2," ".join(te2[j:]))
                    j_last = j
                    break
            else:
                t1 = ts2[j+1]
                t2 = ts2[-1]
                delay = r-(t2+t1)/2
                unchange_latency_num += delay * (t2 - t1)
                unchange_latency_den += t2 - t1
                #print("%.2f"%t1,"%.2f"%t2," ".join(te2[j+1:]))
                j_last = j+1
        i -= 1

    if j_last > 0:
        t1 = data[0]["timestamps"][0]
        t2 = data[0]["timestamps"][:j_last+1][-1]
        r = data[0]["received"]
        delay = r-(t2+t1)/2
        unchange_latency_num += delay * (t2 - t1)
        unchange_latency_den += t2 - t1
        #print("%.2f"%t1,"%.2f"%t2," ".join(d2["text"][:j_last]))

    stable_latency = stable_latency_num / stable_latency_den if stable_latency_den != 0 else np.nan
    unstable_latency = unstable_latency_num / unstable_latency_den if unstable_latency_den != 0 else np.nan
    first_unchange_latency = unchange_latency_num / unchange_latency_den if unchange_latency_den != 0 else np.nan

    return stable_latency, unstable_latency, first_unchange_latency


def calculate_latency_sys_ind(timestamps):
    """
    Calculate the latency in a system-independent way, i.e., not using the audio utterance start time and end time
    provided by the system.

    Average the (timestamp when an output word is received - 0.3 x #prev_output_words).
    0.3 x #prev_output_words in an APPROXIMATION of the previous part of the audio before the word in consideration
    is spoken. Note that this approximation is bad when there are many silence part in the audio

    Note that this is (1) purely relative to compare between systems and (2) only evaluate COMPUTATIONAL latency,
    i.e., only for comparing systems that emit similar output words

    :param timestamps: the timestamp of the start of audio segment, end of audio segment,
    received back translation/transcription, whether the translation/transcription is stable.
    From a single session
    :return:
        stable_latency:  Consider only stable output
        unstable_latency: Consider unstable output that first time stop changing
    """
    stable_word_timestamps = []
    for timestamp in timestamps:
        if not timestamp['unstable']:
            # Repeat the timestamps x times, where x is the number of words in the message
            stable_word_timestamps.extend([timestamp['output_received']]*len(timestamp['text'].split()))

    unstable_word_timestamps = []
    message_blocks = split_to_message_blocks(timestamps)

    # Go through each unstable-stable block
    for single_message_block in message_blocks:
        # Consider each utterance timestamp
        stable_text = single_message_block[-1]['text']
        for i, stable_word in enumerate(stable_text.split()):
            # Go up in the timestamps to see when is this word first outputted, i.e., first unchanged
            first_stable_timestamp = None
            for utt in reversed(single_message_block):
                if (i < len(utt['text'].split())) and \
                        (utt['text'].split()[i] == stable_word):
                    first_stable_timestamp = utt['output_received']
                else:
                    break
            unstable_word_timestamps.append(first_stable_timestamp)

    stable_word_delay = []
    unstable_word_delay = []

    for i, timestamp in enumerate(stable_word_timestamps):
        stable_word_delay.append(timestamp - 0.3*i)
    for i, timestamp in enumerate(unstable_word_timestamps):
        unstable_word_delay.append(timestamp - 0.3*i)

    return np.array(stable_word_delay).mean(), np.array(unstable_word_delay).mean()


def split_to_audio_blocks(timestamps):
    """
    Split the timestamps into groups of messages belong to each session
    :param timestamps
    :return: audio_blocks
    """
    blocks = []
    single_block = []
    prev_audio_start = 0

    for timestamp in timestamps:
        if timestamp['audio_start'] < prev_audio_start:
            # Switch to a new audio, reset single_block
            blocks.append(single_block)
            single_block = []
        single_block.append(timestamp)
        prev_audio_start = timestamp['audio_start']
    blocks.append(single_block)  # Appending the last block

    return blocks


def split_to_message_blocks(timestamps):
    """
    Split the timestamps into groups of messages from unstable to stable
    :param timestamps: From a single session
    :return: message_blocks
    """
    message_blocks = []
    single_message_block = []

    for timestamp in timestamps:
        single_message_block.append(timestamp)
        if not timestamp['unstable']:
            message_blocks.append(single_message_block)
            single_message_block = []

    return message_blocks


def calculate_avg_stablize_time(timestamps):
    """
    Calculate the average time it takes from when an utterance translation has first output until it stops changing
    i.e., stablized
    :param timestamps: From a single session
    :return: stablized time
    """
    count_utt = 0
    total_stablize_time = 0

    message_blocks = split_to_message_blocks(timestamps)

    # Go through each unstable-stable block
    for single_message_block in message_blocks:
        stable_output = single_message_block[-1]['text']

        # Consider each utterance timestamp
        for utt_idx, utt in enumerate(single_message_block):
            if not utt['unstable']:
                # It will never change again
                total_stablize_time += 0
                count_utt += 1
            else:
                # Log it first output timestamp
                first_output_time = utt['output_received']
                first_output_length = len(utt['text'].split())

                # Find the timestamp when it stops changing
                for future_utt in single_message_block[utt_idx+1:]:
                    # Extract the first part of the future utterance,
                    # which correspond to the considering current utterance
                    first_part_of_text = ' '.join(future_utt['text'].split()[:first_output_length])
                    if stable_output.startswith(first_part_of_text):
                        stable_output_time = future_utt['output_received']
                        total_stablize_time += stable_output_time - first_output_time
                        count_utt += 1
                        break

    return total_stablize_time/count_utt


def average_over_sessions(latency_function, timestamps):
    ses_splitted_timestamps = split_to_audio_blocks(timestamps)
    results = []
    for timestamps_per_ses in ses_splitted_timestamps:
        tmp = latency_function(timestamps_per_ses)
        if isinstance(tmp, tuple):
            results.append(tmp)
        else:
            results.append((tmp,))
    averages = [sum(result) / len(result) for result in zip(*results)]
    return averages[0] if len(averages) == 1 else averages


def average_nr_flickers(timestamps, refs):
    """
    :param timestamps: from a single session
    :param refs: the gold standard reference
    :return: total number of flickers / number of words
    i.e., how times a word is updated on average
    """
    ses_splitted_timestamps = split_to_audio_blocks(timestamps)
    results = [calculate_flickers(timestamps_per_ses) for timestamps_per_ses in ses_splitted_timestamps]
    nr_word_ref = sum([len(x.split()) for x in refs])
    avg_flickers = sum(results) / nr_word_ref
    return avg_flickers


def calculate_flickers(timestamps):
    """
    :param timestamps: from a single session
    :return: number of flickers , i.e., how many words are updated in total

    A B C D
    A B E F G
    --> [C D] was flickered
    """
    count_flickers = 0

    data = timestamps_to_website_output(timestamps)

    i = len(data)-1
    j_last = len(data[-1]["text"]) + 1000
    while i > 0:
        d2 = data[i]
        d1 = data[i-1]

        te1 = d1["text"][:j_last]
        te2 = d2["text"][:j_last]

        if te1 != te2:
            #print(te1)
            #print(te2)

            for j,(w1,w2) in enumerate(zip(te1,te2)):
                if w1!=w2:
                    for w1_,w2_ in zip(te1[j:],te2[j:]):
                        if w1_ != w2_:
                            count_flickers += 1
                    if len(te1) - len(te2) > 0:
                        count_flickers += len(te1) - len(te2)
                    #print(" ".join(te2[j:]))
                    j_last = j
                    break
            else:
                #print(" ".join(te2[j+1:]))
                j_last = j+1
        i -= 1

    if j_last > 0:
        pass #print(" ".join(d2["text"][:j_last]))

    return count_flickers


def calculate_flickering_rate(timestamps):
    """
    :param timestamps: from a single session
    :return: flickering_rate, i.e., how many words are updated in a second on average

    A B C D
    A B E F G
    --> [C D] was flickered
    """
    count_flickers = 0
    message_blocks = split_to_message_blocks(timestamps)

    for block in message_blocks:
        for i in range(len(block)-1):
            first_non_overlap_index = find_first_non_overlap_index(block[i]['text'].split(),
                                                                   block[i+1]['text'].split())
            count_flickers = count_flickers + len(block[i]['text'].split()) - first_non_overlap_index

    return count_flickers / timestamps[-1]['output_received']


def find_first_non_overlap_index(x, y):
    """
    :param x: list
    :param y: list
    :return: first_non_overlap_index
     e.g., x=[1,2,3,4,5], y=[1,2,30,40,5], then return 2
    """
    min_length = min(len(x), len(y))
    for i in range(min_length):
        if x[i] != y[i]:
            return i
    return min_length


def binary_str_to_tensor(binary_str):
    return torch.Tensor([int(x) for x in binary_str])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sentences-output-path",
                        help="Path to the resegmented output sentences", type=str, default="")
    parser.add_argument("--lowercased-sentences-output-path",
                        help="Path to the lowercased resegmented output sentences."
                             "Required since resegmentation is done seperately for cased and uncased.", type=str,
                        default="")
    parser.add_argument("--timestamps-output-path",
                        help="Path to the audio and received output timestamps", type=str, default="")
    parser.add_argument('--segment-target-path', type=none_or_str, default=None)
    parser.add_argument('--segment-sysout-path', type=none_or_str, default=None)
    parser.add_argument("--ref-path",
                        help="Path to the reference text for evaluation ", type=str, default="")
    parser.add_argument("--output_lang",
                        help="Output language being evaluated", type=str, default=None)
    parser.add_argument("--metrics", type=str, nargs="*")
    parser.add_argument("--audio_index", type=none_or_int, default=None)
    args = parser.parse_args()

    # Load in the references
    if os.path.isfile(args.ref_path):
        with open(f"{args.ref_path}", 'r') as f:
            refs = f.readlines()
            refs = [ref.strip() for ref in refs]
    else:
        refs = None

    # Load in the output sentences
    if os.path.isfile(args.sentences_output_path):
        with open(f"{args.sentences_output_path}", 'r') as f:
            output_sentences = f.readlines()
            output_sentences = [output_sentence.strip() for output_sentence in output_sentences]
    else:
        output_sentences = None

    # Load in the lowercased output sentences
    if os.path.isfile(args.lowercased_sentences_output_path):
        with open(f"{args.lowercased_sentences_output_path}", 'r') as f:
            lowercased_output_sentences = f.readlines()
            lowercased_output_sentences = [lowercased_output_sentence.strip()
                                           for lowercased_output_sentence in lowercased_output_sentences]
    else:
        lowercased_output_sentences = None

    # Load in the timestamps
    if ('latency_ST' in args.metrics or 'latency_ASR' in args.metrics
        or 'avgFlickers_ST' in args.metrics or 'avgFlickers_ASR' in args.metrics) \
            and os.path.isfile(args.timestamps_output_path):
        with open(f"{args.timestamps_output_path}", 'r') as f:
            timestamps = f.readlines()
            timestamps = [ast.literal_eval(timestamp.strip()) for timestamp in timestamps]
        if len(timestamps) == 0:
            # Empty timestamps file, set to None
            timestamps = None
    else:
        timestamps = None

    if args.audio_index is not None:
        suffix = f'_{args.audio_index}'
    else:
        suffix = ''

    if ('BLEU' in args.metrics) and (output_sentences is not None) and (refs is not None):
        print(f"BLEU{suffix}_{args.output_lang}={calculate_bleu(output_sentences, refs, args.output_lang)}")
    if ('WER' in args.metrics) and (output_sentences is not None) and (refs is not None) and (lowercased_output_sentences is not None):
        print(f"WER{suffix}_{args.output_lang}={calculate_wer(output_sentences, refs)}")
        print(f"WER_wo_case_punct{suffix}_{args.output_lang}={calculate_wer_without_case_punct(lowercased_output_sentences, refs)}")
    if (('latency_ST' in args.metrics) or ('latency_ASR' in args.metrics)) and \
            (timestamps is not None):
        metric = [x for x in args.metrics if x.startswith("latency")][0]
        stable_latency, unstable_latency, first_unchange_latency = average_over_sessions(calculate_latency, timestamps)
        print(f"stable_{metric}{suffix}_{args.output_lang}={stable_latency}")
        print(f"unstable_{metric}{suffix}_{args.output_lang}={unstable_latency}")
        print(f"firstUnchange_{metric}{suffix}_{args.output_lang}={first_unchange_latency}")
    if (('avgStablizeTime_ST' in args.metrics) or ('avgStablizeTime_ASR' in args.metrics)) and \
            (timestamps is not None):
        metric = [x for x in args.metrics if x.startswith("avgStablizeTime")][0]
        avgStablizeTime = average_over_sessions(calculate_avg_stablize_time, timestamps)
        print(f"{metric}{suffix}_{args.output_lang}={avgStablizeTime}")
    if (('latencySysInd_ST' in args.metrics) or ('latencySysInd_ASR' in args.metrics)) and \
            (timestamps is not None):
        metric = [x for x in args.metrics if x.startswith("latencySysInd")][0]
        stable_latency_sysInd, unstable_latency_sysInd = average_over_sessions(calculate_latency_sys_ind, timestamps)
        print(f"stable_{metric}{suffix}_{args.output_lang}={stable_latency_sysInd}")
        print(f"unstable_{metric}{suffix}_{args.output_lang}={unstable_latency_sysInd}")
    if (('avgFlickers_ST' in args.metrics) or ('avgFlickers_ASR' in args.metrics)) and \
            (timestamps is not None) and (refs is not None):
        metric = [x for x in args.metrics if x.startswith("avgFlickers")][0]
        avg_flickers = average_nr_flickers(timestamps, refs)
        print(f"{metric}{suffix}_{args.output_lang}={avg_flickers}")

        # Also report the flickering rate
        metric = metric.replace('avgFlickers', 'flickeringRate')
        flickering_rate = average_over_sessions(calculate_flickering_rate, timestamps)
        print(f"{metric}{suffix}_{args.output_lang}={flickering_rate}")

    if 'Chaptering' in args.metrics and args.segment_sysout_path is not None and args.segment_target_path is not None:
        with open(args.segment_sysout_path, 'r') as f:
            output = f.read()
        with open(args.segment_target_path, 'r') as f:
            target = f.read()

        scores = calc_metrics(
            output=[binary_str_to_tensor(output)],
            encoded_targets=[binary_str_to_tensor(target)],
            targets=[target],
            lengths=torch.Tensor([binary_str_to_tensor(output).shape[0]])
        )

        for metric, score in scores.items():
            print(f"Chaptering_{metric}{suffix}_{args.output_lang}={score}")
