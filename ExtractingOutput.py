#!/usr/bin/env python3
import argparse
import json
import ast
from tokenization import tokenize
from generate_chapter_targets import sent_tokenize


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Extract the system translation and timestamps from the json output."
                                     "Timestamp only works for audio-input tasks.")
    parser.add_argument("--raw_output",
                        help="Path to the raw json system output", type=str)
    parser.add_argument("--sentences_output",
                        help="Location to store the extracted output sentences", type=str)
    parser.add_argument("--timestamps_output",
                        help="Location to store the extracted audio segment timestamps and message received timestamps,"
                             "along with whether the message at that point is stable",
                        type=str)
    parser.add_argument("--segment_output",
                        help="Location to store the extracted segment breaks.",
                        type=str)
    parser.add_argument("--raw_output_type",
                        type=str,
                        choices=[
                            'MT', 'cascadedST', 'e2eST', 'ASR',
                            'ASR_byProd_cascadedST',  # The byproduct ASR result from the ASR component of cascadedST
                            'ASR_byProd_e2eST',
                            'ASRChaptering'])  # The byproduct ASR result from the multilingual e2eST model
    parser.add_argument("--src_lang")
    parser.add_argument("--tgt_lang")
    args = parser.parse_args()

    # Load in the output json
    if args.raw_output_type == 'MT':
        with open(f"{args.raw_output}") as sys_output_file:
            sys_output_infos = json.load(sys_output_file)
            sys_output_infos = [json.loads(x) for x in sys_output_infos]
    else:
        with open(f"{args.raw_output}", 'r') as sys_output_file:
            sys_output_infos = sys_output_file.readlines()
            sys_output_infos = [x.strip() for x in sys_output_infos]
            sys_output_infos = [x.split('▁') for x in sys_output_infos]
            received_timestamps = [float(x[0]) for x in sys_output_infos]
            sys_output_infos = [json.loads(x[1]) for x in sys_output_infos]

    # Extract the translated sentences
    sys_output_sentences = []
    timestamps = []
    relevant_worker = None

    output_lang = args.src_lang if args.raw_output_type.startswith('ASR') else args.tgt_lang
    if output_lang in ['en', 'de', 'fr', 'it', 'nl', 'es', 'pt']:
        display_langs = [output_lang, '7eu']
    else:
        display_langs = [output_lang]

    start = 0
    # Find the start timestamp of each session (each audio sent)
    for i, sys_output_info in enumerate(sys_output_infos):
        if ('controll' in sys_output_info.keys() and sys_output_info['controll'] == 'START') and \
                (sys_output_info['sender'] == 'asr:0'):
            start = float(received_timestamps[i])

        received_timestamps[i] = received_timestamps[i] - start

    for i, sys_output_info in enumerate(sys_output_infos):
        if (args.raw_output_type == 'e2eST' or args.raw_output_type.startswith('ASR')) and \
                ('controll' in sys_output_info.keys() and sys_output_info['controll'] == 'INFORMATION'):
            if ('sender' in sys_output_info.keys()
                    and sys_output_info['sender'].startswith('asr')  # Output from an ASR
                    and 'display_language' in sys_output_info[sys_output_info['sender']]
                    and (sys_output_info[sys_output_info['sender']]['display_language'] in display_langs or
                         sys_output_info[sys_output_info['sender']]['display_language'] == sys_output_info[sys_output_info['sender']]['language'])  # Desired lang output
            ):
                relevant_worker = sys_output_info['sender']
        elif (args.raw_output_type in ['MT', 'cascadedST']) and \
                ('controll' in sys_output_info.keys() and sys_output_info['controll'] == 'INFORMATION'):
            if ('sender' in sys_output_info.keys()
                    and sys_output_info['sender'].startswith('mt')  # Output from an MT
                    and 'display_language' in sys_output_info[sys_output_info['sender']]
                    and sys_output_info[sys_output_info['sender']]['display_language'] in display_langs  # Desired lang output
            ):
                relevant_worker = sys_output_info['sender']


        is_relevant_output = (sys_output_info['sender'] == relevant_worker)

        if 'seq' in sys_output_info.keys() and is_relevant_output:
            if not sys_output_info['unstable']:
                # Store the final stable transcribe/translated sentences
                # For zh, ja, ko, have to tokenize so that mwerSegmenter can work later on
                if output_lang in ['zh', 'ja', 'ko']:
                    text = tokenize(output_lang, sys_output_info['seq'])
                else:
                    text = sys_output_info['seq']
                sys_output_sentences.append(text)

            if args.raw_output_type != 'MT':
                try:
                    # Store the audio segment timestamps and message received timestamps
                    timestamp = {'audio_start': float(sys_output_info['start']),
                                 'audio_end': float(sys_output_info['end']),
                                 'output_received': float(received_timestamps[i]),
                                 'unstable': bool(sys_output_info['unstable']),
                                 'text': sys_output_info['seq']
                                 }
                except Exception as e:
                    print(f"Invalid timestamp at index {i}")
                    print("Exception details:", e)
                else:
                    timestamps.append(timestamp)

        if (args.raw_output_type == 'ASRChaptering') and \
                ('markup' in sys_output_info.keys()) and \
                (sys_output_info['markup'] == 'chapterBreak'):
            sys_output_sentences.append('<br /><br />')

    if args.raw_output_type == 'ASRChaptering':
        # Tokenize to sentences based on punctuation
        sentences = sent_tokenize(' '.join(sys_output_sentences))
        sentences = [x for x in sentences if x != '<br /><br />']

        segment_output = ''
        for sent in sentences:
            if '<br /><br />' in sent:
                segment_output = segment_output + '1'
            else:
                segment_output = segment_output + '0'
            with open(f"{args.segment_output}", 'w') as f:
                f.write(f"{segment_output}")

        sys_output_sentences = [x for x in sys_output_sentences if x != '<br /><br />']

    # Save the extracted sentences to file
    with open(f"{args.sentences_output}", 'w') as f:
        for sys_output_sentence in sys_output_sentences:
            f.write(f"{sys_output_sentence}\n")

    # Save the extracted timestamps to file
    if args.raw_output_type != 'MT':
        with open(f"{args.timestamps_output}", 'w') as f:
            for timestamp in timestamps:
                f.write(f"{timestamp}\n")
