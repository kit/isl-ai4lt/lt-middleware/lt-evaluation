#!/bin/bash

if [ -z "$1" ]; then
  configs_path=configs
else
  configs_path=$1
fi

# Store different settings to seperate files in configs/ folder
mkdir -p ${configs_path}
rm ${configs_path}/*.sh
declare -a datanames=("covost2-test-en-de" "IWSLT2019-test-en-de" "IWSLT2014-test-de-en" "IWSLT.ACLdev2023-en-de" "IWSLT.dev2012-de-en" "LT.CS.2012-de-en" "LT.nonCS.2012-de-en" )
declare -a use_preps=("True" "False")
declare -a asr_servers=("default" "Whisper" )

config_counter=0
for dataname in ${datanames[@]}; do
  # Evaluate MT
  echo "dataname=${dataname}
task=MT
MTmode=SendStable" >> ${configs_path}/config${config_counter}.sh
  config_counter=$((config_counter+1))

  # Evaluate end-to-end ST
  for use_prep in ${use_preps[@]}; do
    # Offline settings
    declare -a segmenters=("VAD" "SHAS" )
    for segmenter in ${segmenters[@]}; do
      echo "dataname=${dataname}
task=e2eST
segmenter=${segmenter}
ffmpeg_speed=-1
version=offline
stability_detection=False
ASRmode=SendStable
use_prep=${use_prep}" >> ${configs_path}/config${config_counter}.sh
      config_counter=$((config_counter+1))
    done

    # Online settings
    declare -a stability_detections=("True" "False" )
    declare -a ASRmodes=("SendStable" "SendUnstable" )
    for stability_detection in ${stability_detections[@]}; do
      for ASRmode in ${ASRmodes[@]}; do
        echo "dataname=${dataname}
task=e2eST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
use_prep=${use_prep}" >> ${configs_path}/config${config_counter}.sh
    config_counter=$((config_counter+1))
      done
    done
  done


  # Evaluate cascaded ST
  for use_prep in ${use_preps[@]}; do
    for asr_server in ${asr_servers[@]}; do
      # Offline settings
      declare -a segmenters=("VAD" "SHAS" )
      for segmenter in ${segmenters[@]}; do
        echo "dataname=${dataname}
task=cascadedST
segmenter=${segmenter}
ffmpeg_speed=-1
version=offline
stability_detection=False
ASRmode=SendStable
MTmode=SendStable
use_prep=${use_prep}
asr_server=${asr_server}" >> ${configs_path}/config${config_counter}.sh
        config_counter=$((config_counter+1))
      done

      # Online settings
      declare -a stability_detections=( "True" )
      declare -a resendings=("True" "False" )
      for stability_detection in ${stability_detections[@]}; do
        for resending in ${resendings[@]}; do
          if [[ $resending == "True" ]]; then
            ASRmode="SendUnstable"
            MTmode="SendUnstable"
          else
            ASRmode="SendStable"
            MTmode="SendPartial"
          fi
          echo "dataname=${dataname}
task=cascadedST
segmenter=VAD
ffmpeg_speed=1
version=online
stability_detection=${stability_detection}
ASRmode=${ASRmode}
MTmode=${MTmode}
use_prep=${use_prep}
asr_server=${asr_server}
resending=${resending}" >> ${configs_path}/config${config_counter}.sh
      config_counter=$((config_counter+1))
        done
      done
    done
  done
done